package services.soap.tests;

import static utilities.SOAPUI_Request.getSoapServiceResponse;
import static verify.SoftAssertions.verifyContains;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import utilities.InitTests;
import static utilities.MyExtentReports.reports;

public class TestSOAPWebService extends InitTests
{
	
	@Test()
	public static void testSOAPService() throws IOException
	{
		ExtentTest test=null;
		try {
			
			test = reports.createTest("testSOAPService");
			test.assignCategory("SOAP");
			String wsdlUrl = "http://usfkl13as343v/TaskManager2_QA_HRI_Temp/TaskManagerApi.svc?wsdl";
			String requestUrl = "http://tempuri.org/ITaskManagerApi/SubmitTask";
			String requestFilePath= dir_path+"\\src\\main\\resources\\testdata\\HRI_FileProcess_SoapUI.xml";
			String response=getSoapServiceResponse(wsdlUrl,requestUrl, requestFilePath);
			System.out.println("response"+response);
			verifyContains(response, "TaskId", "",test);
			verifyContains(response, "TaskSeqNo", "",test);


		} catch (Error e) {
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			reports.flush();
		
		}
		
		
}
	
}

package regression.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.regression.DashboardPage;
import pages.regression.EstimatePage;
import pages.regression.LoginPage;
import pages.regression.ROL_Page;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_ReqKit extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_ReqKit(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_ReqKit rkit = new Verify_ReqKit("RK");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true, priority = 1)
	public void verifyReqKit() throws Exception {

		try {

			test = reports.createTest("verifyRequestKit");
			test.assignCategory("regression");

			driver = driverFact.getEventDriver(webdriver, test);
			
			//-----------------------------------------Login Flow-------------------------------------------//
			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.BWuserName);
			verifyElementTextContains(login.BWLogLabel, "Log in to your existing account.", test);
			verifyElementTextContains(login.BWforgotTxt, "Forgot User Name or Password?", test);
			login.BWloginDB(USERNAME, PASSWORD);
			
			//---------------------------------------Dashboard Flow-----------------------------------------//
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(dashboard.DashboardTab);
			verifyElementTextContains(dashboard.ADPTile, "ADP Voluntary Early Retirement Program", test);
			verifyElementTextContains(dashboard.BenefitCommencementTile, "Benefit Commencement", test);
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			
			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);
			clickElement(dashboard.DashboardTab);
			
			verifyElementTextContains(dashboard.BenefitCommencementTile, "Benefit Commencement", test);
			waitForElementToDisplay(dashboard.GetStartedBtn);
			clickElement(dashboard.GetStartedBtn);
			
			//---------------------------------------Request Kit Flow-----------------------------------------//
			ROL_Page rolpg = new ROL_Page(driver);
			waitForElementToDisplay(rolpg.BenefitCommencementLabel);
			verifyElementTextContains(rolpg.BenefitCommencementLabel, "Benefit Commencement", test);
			verifyElementTextContains(rolpg.BenefitCommOverviewLabel, "Benefit Commencement", test);
			verifyElementTextContains(rolpg.StatusDetailsLink, "Status Details", test);
			clickElement(rolpg.StatusDetailsLink);
			
			verifyElementTextContains(rolpg.BenCommStepsLabel, "Benefit Commencement Steps", test);
			verifyElementTextContains(rolpg.StepOneLabel, "1", test);
			verifyElementTextContains(rolpg.StepTwoLabel, "2", test);
			verifyElementTextContains(rolpg.StepThreeLabel, "3", test);
			
			clickElement(rolpg.BackOVBtn);
						
			waitForElementToDisplay(rolpg.BenefitCommencementLabel);
			verifyElementTextContains(rolpg.BenefitCommencementLabel, "Benefit Commencement", test);
			verifyElementTextContains(rolpg.BenefitCommOverviewLabel, "Benefit Commencement", test);
			waitForElementToDisplay(rolpg.GetStartedRKBtn);
			verifyElementTextContains(rolpg.GetStartedRKBtn, "Get Started", test);
			
			clickElement(rolpg.GetStartedRKBtn);
			waitForElementToDisplay(rolpg.ReqBenCommKitLabel);
			verifyElementTextContains(rolpg.ReqBenCommKitLabel, "Request a Benefit Commencement Kit", test);
			
			rolpg.requestKit("Cancel", test);
			
			//-----------------------------------------Logout Flow-------------------------------------------//
			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyReqKit()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyReqKit()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}

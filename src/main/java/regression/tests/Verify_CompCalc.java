package regression.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementIsNotPresent;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.regression.DashboardPage;
import pages.regression.EstimatePage;
import pages.regression.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_CompCalc extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_CompCalc(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_CompCalc cmpcal = new Verify_CompCalc("CompCalc");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true)
	public void verifyCompCalc() throws Exception {

		try {
			test = reports.createTest("verifyCompCalc");
			test.assignCategory("regression");

			driver = driverFact.getEventDriver(webdriver, test);
			
			//-----------------------------------------Login Flow-------------------------------------------//
			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.userName);
			verifyElementTextContains(login.userLabel, "username", test);
			verifyElementTextContains(login.passwordLabel, "password", test);
			verifyElementTextContains(login.forgotTxt, "Forgot username or password?", test);
			login.loginDB(USERNAME, PASSWORD);
			
			//---------------------------------------Dashboard Flow-----------------------------------------//
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(dashboard.DashboardTab);
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			verifyElementTextContains(dashboard.RetirementPlanningTile, "RETIREMENT PLANNING", test);
			
			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);

			scrollToElement(driver, dashboard.MenuBtn);
			clickElement(dashboard.MenuBtn);
			
			//-----------------------------------Home Link Verification-------------------------------------//
			waitForElementToDisplay(dashboard.HomeBtn);
			waitForElementToDisplay(dashboard.MyAccountLabel);
			verifyElementTextContains(dashboard.MyAccountLabel, "My Account", test);
			verifyElementTextContains(dashboard.Forms_DocLabel, "Forms & Documents", test);
			verifyElementTextContains(dashboard.KnowledgeCenterLabel, "Knowledge Center", test);
			verifyElementTextContains(dashboard.UploadDocsLabel, "UPLOAD DOCUMENTS", test);
			
			//clickElement(dashboard.KelloggHeritageLink);
			clickElement(dashboard.OverviewLink_hm);
			
			//---------------------------------------Estimate Page Flow-----------------------------------------//
			EstimatePage Estimate = new EstimatePage(driver);
			waitForElementToDisplay(Estimate.YourPlanSummaryLabel);
			verifyElementTextContains(Estimate.YourPlanSummaryLabel, "Your Plan Summary", test);

			clickElementUsingJavaScript(driver, Estimate.EstimatesTab);
			waitForElementToDisplay(Estimate.EstimateYourPensionLink);
			verifyElementTextContains(Estimate.EstimateYourPensionLink, "Estimate Your Pension", test);
			waitForElementToDisplay(Estimate.EstimateYourPensionLabel);
			verifyElementTextContains(Estimate.EstimateYourPensionLabel, "Estimate Your Pension", test);

			clickElement(Estimate.SavedEstimatesLink);
			waitForElementToDisplay(Estimate.SavedEstimatesLabel);
			verifyElementTextContains(Estimate.SavedEstimatesLabel, "Saved Estimates", test);

			clickElement(Estimate.MyDataTab);
			waitForElementToDisplay(Estimate.MyDataLabel);
			verifyElementTextContains(Estimate.MyDataLabel, "My Data", test);

			//clickElement(Estimate.MyElectionsTab);
			//waitForElementToDisplay(Estimate.MyElectionsLabel);
			//verifyElementTextContains(Estimate.MyElectionsLabel, "My Elections", test);
			
			clickElementUsingJavaScript(driver, Estimate.EstimatesTab);
			waitForElementToDisplay(Estimate.EstimateYourPensionLink);
			verifyElementTextContains(Estimate.EstimateYourPensionLink, "Estimate Your Pension", test);
			clickElement(Estimate.SavedEstimatesLink);
			waitForElementToDisplay(Estimate.SavedEstimatesLabel);
			verifyElementTextContains(Estimate.SavedEstimatesLabel, "Saved Estimates", test);
			
			//-------------------------------Compare Calc Flow for 2 Estimates------------------------------//
			clickElement(Estimate.Calc1_ChckBx);
			clickElement(Estimate.Calc2_ChckBx);
			waitForElementToDisplay(Estimate.CompareButton);
			clickElement(Estimate.CompareButton);
			
			waitForElementToDisplay(Estimate.EstimateComparisonLabel);
			verifyElementTextContains(Estimate.EstimateComparisonLabel, "Estimate Comparison", test);
			waitForElementToDisplay(Estimate.YourEstimateComparisonLabel);
			verifyElementTextContains(Estimate.YourEstimateComparisonLabel, "Your Estimate Comparison", test);
			
			waitForElementToDisplay(Estimate.ViewAssumptionButton);
			clickElement(Estimate.ViewAssumptionButton);
			waitForElementToDisplay(Estimate.AssumptionHeaderLabel);
			verifyElementTextContains(Estimate.AssumptionHeaderLabel, "Assumptions Used for Displayed Pension Estimates", test);
			waitForElementToDisplay(Estimate.LDWAssumption);
			verifyElementTextContains(Estimate.LDWAssumption, "LAST DAY WORKED", test);
			waitForElementToDisplay(Estimate.AgeAtLDWAssumption);
			verifyElementTextContains(Estimate.AgeAtLDWAssumption, "AGE AT LAST DAY WORKED", test);
			
			verifyElementIsPresent(Estimate.CalcAnotherEstimateButton, test, "Calculate Another Estimate Button");
			
			clickElement(Estimate.CompareBackButton);
			
			//-------------------------------Compare Calc Flow for 3 Estimates------------------------------//
			waitForElementToDisplay(Estimate.SavedEstimatesLabel);
			verifyElementTextContains(Estimate.SavedEstimatesLabel, "Saved Estimates", test);
			clickElement(Estimate.Calc3_ChckBx);
			waitForElementToDisplay(Estimate.CompareButton);
			clickElement(Estimate.CompareButton);
			
			waitForElementToDisplay(Estimate.EstimateComparisonLabel);
			verifyElementTextContains(Estimate.EstimateComparisonLabel, "Estimate Comparison", test);
			waitForElementToDisplay(Estimate.YourEstimateComparisonLabel);
			verifyElementTextContains(Estimate.YourEstimateComparisonLabel, "Your Estimate Comparison", test);
			
			waitForElementToDisplay(Estimate.ViewAssumptionButton);
			clickElement(Estimate.ViewAssumptionButton);
			waitForElementToDisplay(Estimate.AssumptionHeaderLabel);
			verifyElementTextContains(Estimate.AssumptionHeaderLabel, "Assumptions Used for Displayed Pension Estimates", test);
			waitForElementToDisplay(Estimate.LDWAssumption);
			verifyElementTextContains(Estimate.LDWAssumption, "LAST DAY WORKED", test);
			waitForElementToDisplay(Estimate.AgeAtLDWAssumption);
			verifyElementTextContains(Estimate.AgeAtLDWAssumption, "AGE AT LAST DAY WORKED", test);
	
			verifyElementIsNotPresent(Estimate.CalcAnotherEstimateButton, test, "Calculate Another Estimate Button");
						
			//-----------------------------------------Logout Flow------------------------------------------//
			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyCompCalc()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyCompCalc()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}

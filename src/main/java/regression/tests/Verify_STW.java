package regression.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyElementIsPresent;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.regression.DashboardPage;
import pages.regression.InPayPage;
import pages.regression.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_STW extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_STW(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_STW stw = new Verify_STW("STW");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true)
	public void verifySTW() throws Exception {

		try {
			test = reports.createTest("verifySTW");
			test.assignCategory("regression");

			driver = driverFact.getEventDriver(webdriver, test);
			
			//-----------------------------------------Login Flow-------------------------------------------//
			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.userName);
			verifyElementTextContains(login.userLabel, "username", test);
			verifyElementTextContains(login.passwordLabel, "password", test);
			verifyElementTextContains(login.forgotTxt, "Forgot username or password?", test);
			login.loginDB(USERNAME, PASSWORD);
			
			//---------------------------------------Dashboard Flow-----------------------------------------//
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(dashboard.DashboardTab, driver, 400);
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			verifyElementTextContains(dashboard.RetirementPlanningTile, "RETIREMENT PLANNING", test);
			
			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);

			scrollToElement(driver, dashboard.MenuBtn);
			clickElement(dashboard.MenuBtn);
			
			//-----------------------------------Home Link Verification-------------------------------------//
			waitForElementToDisplay(dashboard.HomeBtn);
			waitForElementToDisplay(dashboard.MyAccountLabel);
			verifyElementTextContains(dashboard.MyAccountLabel, "My Account", test);
			verifyElementTextContains(dashboard.ProfileLink, "Profile", test);
			verifyElementTextContains(dashboard.CommunicationsLink, "Communication", test);
			verifyElementTextContains(dashboard.BeneficiariesLink, "Beneficiaries", test);
			verifyElementTextContains(dashboard.Forms_DocLabel, "Forms & Documents", test);
			verifyElementTextContains(dashboard.Retirement_PensionLink, "Retirement Pension", test);
			verifyElementTextContains(dashboard.KnowledgeCenterLabel, "Knowledge Center", test);
			verifyElementTextContains(dashboard.RetirementLink, "Retirement", test);
			verifyElementTextContains(dashboard.MoneyManagementLink, "Money Management", test);
			verifyElementTextContains(dashboard.UploadDocsLabel, "UPLOAD DOCUMENTS", test);
			verifyElementTextContains(dashboard.GeneralDocumentsLink, "General Documents", test);
			verifyElementTextContains(dashboard.TaxWithHoldinglink_hm, "Tax Withholding", test);
			verifyElementTextContains(dashboard.PaymentMethodlink_hm, "Payment Method", test);
			
			clickElement(dashboard.OverviewLink_hm);
			
			//-----------------------------------------Inpay Flow-------------------------------------------//
			InPayPage Inpay = new InPayPage(driver);
			waitForElementToDisplay(Inpay.PaymentSummaryLabel);
			verifyElementTextContains(Inpay.PaymentSummaryLabel, "Payment Summary", test);
			clickElement(Inpay.PaymentHistoryLink);
			waitForElementToDisplay(Inpay.PaymentHistoryLabel);
			verifyElementTextContains(Inpay.PaymentHistoryLabel, "Payment History", test);
			clickElement(Inpay.ScheduledDeductionsLink);
			waitForElementToDisplay(Inpay.ScheduledDeductionsLabel);
			verifyElementTextContains(Inpay.ScheduledDeductionsLabel, "Scheduled Deductions", test);
			
			clickElement(Inpay.TaxWithHoldingTab);
			waitForElementToDisplay(Inpay.FederalTaxWithHoldingLabel);
			verifyElementTextContains(Inpay.FederalTaxWithHoldingLabel, "Federal Tax Withholding", test);
			
			//-----------------------------------------State Flow-------------------------------------------//
			clickElement(Inpay.StateLink);
			waitForElementToDisplay(Inpay.StateTaxWithHoldingLabel);
			verifyElementTextContains(Inpay.StateTaxWithHoldingLabel, "State Tax Withholding", test);
			
			waitForElementToDisplay(Inpay.SEditbutton);
			verifyElementTextContains(Inpay.SEditbutton, "Edit", test);
			verifyElementTextContains(Inpay.DownloadWSlink, "Download Worksheet", test);
			verifyElementIsPresent(Inpay.Fedtable, test, "State Detail Table");
			
			verifyElementIsPresent(Inpay.Federalpaymenttitle, test, "State Detail Table Element");
			verifyElementIsPresent(Inpay.FGrossamounttitle, test, "State Detail Table Element");
			verifyElementIsPresent(Inpay.FWithholdingtitle, test, "State Detail Table Element");
			verifyElementIsPresent(Inpay.FAllowancestitle, test, "State Detail Table Element");
			verifyElementIsPresent(Inpay.FMaritalStatustitle, test, "State Detail Table Element");
			verifyElementIsPresent(Inpay.FAdditionalamounttitle, test, "State Detail Table Element");
			
			//--------------------------------------State Change Flow----------------------------------------//
			Inpay.stateChange("New York", "Save", test);
			Inpay.stateChange("Alaska", "Save", test);
						
			
			//-----------------------------------------Logout Flow------------------------------------------//
			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("VerifySTW()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("VerifySTW()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}

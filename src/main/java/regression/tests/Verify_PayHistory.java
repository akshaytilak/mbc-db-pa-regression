package regression.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyElementIsPresent;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.regression.DashboardPage;
import pages.regression.InPayPage;
import pages.regression.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_PayHistory extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_PayHistory(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_PayHistory phis = new Verify_PayHistory("PHis");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true)
	public void verifyPayHistory() throws Exception {

		try {
			test = reports.createTest("verifyPayHistory");
			test.assignCategory("regression");

			driver = driverFact.getEventDriver(webdriver, test);
			
			//-----------------------------------------Login Flow-------------------------------------------//
			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.userName);
			verifyElementTextContains(login.userLabel, "username", test);
			verifyElementTextContains(login.passwordLabel, "password", test);
			verifyElementTextContains(login.forgotTxt, "Forgot username or password?", test);
			login.loginDB(USERNAME, PASSWORD);
			
			//---------------------------------------Dashboard Flow-----------------------------------------//
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(dashboard.DashboardTab, driver, 400);
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			verifyElementTextContains(dashboard.RetirementPlanningTile, "RETIREMENT PLANNING", test);
			
			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);

			scrollToElement(driver, dashboard.MenuBtn);
			clickElement(dashboard.MenuBtn);
			
			//-----------------------------------Home Link Verification-------------------------------------//
			waitForElementToDisplay(dashboard.HomeBtn);
			waitForElementToDisplay(dashboard.MyAccountLabel);
			verifyElementTextContains(dashboard.MyAccountLabel, "My Account", test);
			verifyElementTextContains(dashboard.Forms_DocLabel, "Forms & Documents", test);
			verifyElementTextContains(dashboard.KnowledgeCenterLabel, "Knowledge Center", test);
			verifyElementTextContains(dashboard.UploadDocsLabel, "UPLOAD DOCUMENTS", test);
			
			clickElement(dashboard.OverviewLink_hm);
			
			//-----------------------------------------Inpay Flow-------------------------------------------//
			InPayPage Inpay = new InPayPage(driver);
			waitForElementToDisplay(Inpay.PaymentSummaryLabel);
			verifyElementTextContains(Inpay.PaymentSummaryLabel, "Payment Summary", test);
			clickElement(Inpay.PaymentHistoryLink);
			waitForElementToDisplay(Inpay.PaymentHistoryLabel);
			verifyElementTextContains(Inpay.PaymentHistoryLabel, "Payment History", test);
			clickElement(Inpay.ScheduledDeductionsLink);
			waitForElementToDisplay(Inpay.ScheduledDeductionsLabel);
			verifyElementTextContains(Inpay.ScheduledDeductionsLabel, "Scheduled Deductions", test);
			
			clickElement(Inpay.PaymentHistoryLink);
			waitForElementToDisplay(Inpay.PaymentHistoryLabel);
			verifyElementTextContains(Inpay.PaymentHistoryLabel, "Payment History", test);
			
			//---------------------------------------Pay History Flow----------------------------------------//
			Inpay.check_WTDH(test);
			verifyElementIsPresent(Inpay.PaymentPlanDropDown, test, "Payment Plan DropDown");
			verifyElementTextContains(Inpay.PaymentStartDateLabel, "Payment Start:", test);
			verifyElementIsPresent(Inpay.PaymentStartDate, test, "Payment Start Date");
			verifyElementIsPresent(Inpay.CurrentHistoryDetails, test, "Current History Details");
			verifyElementTextContains(Inpay.CurHis_YearLabel, "Year", test);
			verifyElementTextContains(Inpay.CurHis_GPALabel, "Gross Payment Amount", test);
			verifyElementTextContains(Inpay.CurHis_PMLabel, "Payment Method", test);
			verifyElementTextContains(Inpay.CurHis_DPLabel, "Detailed Report", test);
			
			//-----------------------------------------Logout Flow------------------------------------------//
			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("VerifyPayHistory()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("VerifyPayHistory()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}

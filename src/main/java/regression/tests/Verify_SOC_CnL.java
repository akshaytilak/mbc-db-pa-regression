package regression.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.refreshpage;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.regression.DashboardPage;
import pages.regression.LoginPage;
import pages.regression.PensionAdmin;
import pages.regression.ROL_Page;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_SOC_CnL extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_SOC_CnL(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_SOC_CnL soccl = new Verify_SOC_CnL("SOCCnL");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true, priority = 1)
	public void verifyROL_SOC_CnL() throws Exception {

		try {
			test = reports.createTest("verifyROL_SOC_CnL");
			test.assignCategory("regression");

			driver = driverFact.getEventDriver(webdriver, test);
			
			//-----------------------------------------Login Flow-------------------------------------------//
			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.userName);
			verifyElementTextContains(login.userLabel, "username", test);
			verifyElementTextContains(login.passwordLabel, "password", test);
			verifyElementTextContains(login.forgotTxt, "Forgot username or password?", test);
			login.loginDB(USERNAME, PASSWORD);
			
			//---------------------------------------Dashboard Flow-----------------------------------------//
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(dashboard.DashboardTab);
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			verifyElementTextContains(dashboard.BenefitCommencementTile, "Benefit Commencement", test);
			verifyElementTextContains(dashboard.RetirementPlanningTile, "RETIREMENT PLANNING", test);

			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);
			clickElement(dashboard.DashboardTab);
			verifyElementTextContains(dashboard.BenefitCommencementTile, "Benefit Commencement", test);
			
			//----------------------------------------ROL SOC Flow-------------------------------------------//
			ROL_Page rolpg = new ROL_Page(driver);
			PensionAdmin pad = new PensionAdmin(driver);
			int actStep = rolpg.StatusStep();
			if(actStep>=5) {
				test.pass("Commencement was already Completed, Status : "+actStep+" of 7."
						+ "\nWill cancel it and Test the Flow completely.");
				pad.cancelCommencement(driver, PensionAdminURL, SSN, test);
				refreshpage();
				dashboard.logoutDB();
				waitForElementToDisplay(login.userName);
				login.loginDB(USERNAME, PASSWORD);
				waitForElementToDisplay(dashboard.DashboardTab);
				verifyElementTextContains(dashboard.NotStartedLabel, "Not Started", test);
			}else if(actStep<5 && actStep>=1) {
				test.pass("Commencement Incompleted, Status : "+actStep+" of 7. "
						+ "\nWill cancel it and Test the Flow completely.");
				pad.cancelCommencement(driver, PensionAdminURL, SSN, test);
				refreshpage();
				dashboard.logoutDB();
				waitForElementToDisplay(login.userName);
				login.loginDB(USERNAME, PASSWORD);
				waitForElementToDisplay(dashboard.DashboardTab);
				verifyElementTextContains(dashboard.NotStartedLabel, "Not Started", test);
			}else if(isElementExisting(driver,dashboard.ResumeBtn)) {
				test.pass("Commencement Incompleted, Status : Not Started but Completed the Agreement."
						+ "\nWill cancel it and Test the Flow completely.");
				pad.cancelCommencement(driver, PensionAdminURL, SSN, test);
				refreshpage();
				dashboard.logoutDB();
				waitForElementToDisplay(login.userName);
				login.loginDB(USERNAME, PASSWORD);
				waitForElementToDisplay(dashboard.DashboardTab);
				verifyElementTextContains(dashboard.NotStartedLabel, "Not Started", test);
			}else {
				verifyElementTextContains(dashboard.NotStartedLabel, "Not Started", test);
			}
			
			rolpg.SOC_FP_Flow(driver, "NQ", test);
			rolpg.SOC_Step1_Flow(driver, test);
			
			if(isElementExisting(driver, rolpg.BenefitCommencementLabel)) {
				pad.changeCalcStatus(driver, PensionAdminURL, SSN, "CPP", test);
				refreshpage();
				dashboard.logoutDB();
				waitForElementToDisplay(login.userName);
				login.loginDB(USERNAME, PASSWORD);
				waitForElementToDisplay(dashboard.DashboardTab);
				clickElement(dashboard.ResumeBtn);
			}else {
				if(isElementExisting(driver, rolpg.YPOHBC_Header)) {
					waitForElementToDisplay(rolpg.YPOHBC_Header);
					verifyElementTextContains(rolpg.YPOHBC_Header, "Your Payment Options Have Been Calculated", test);
					clickElement(rolpg.ViewResultsBtn);
				}else {
					waitForElementToDisplay(rolpg.SelectPaymentTypeHeader);
				}
			}
						
			rolpg.SOC_Step2_4_Flow(driver, test);
			pad.DocumentApproval(driver, PensionAdminURL, SSN, "CPP", test);
			
			refreshpage();
			dashboard.logoutDB();
			waitForElementToDisplay(login.userName);
			login.loginDB(USERNAME, PASSWORD);
			waitForElementToDisplay(dashboard.DashboardTab);
			clickElement(dashboard.ResumeBtn);
			rolpg.SOC_Step5_Flow(driver, test);
			clickElement(dashboard.MenuBtn);
			waitForElementToDisplay(dashboard.HomeBtn);
			clickElement(dashboard.HomeBtn);
			waitForElementToDisplay(dashboard.DashboardTab);
			
			actStep = rolpg.StatusStep();
			if(actStep>=5) {
				test.pass("Commencement Completed, Status : "+actStep+" of 7 Completed");
			}
			else {
				test.fail("Commencement Incomplete, Status : "+actStep+" of 7 Completed");
			}
			
			pad.cancelCommencement(driver, PensionAdminURL, SSN, test);
							
			//-----------------------------------------Logout Flow-------------------------------------------//
			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyROL_SOC_CnL", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyROL_SOC_CnL", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}

package regression.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyElementIsPresent;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.regression.DashboardPage;
import pages.regression.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_MP extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_MP(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_MP mp = new Verify_MP("MP");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true, priority = 1)
	public void verifyMP() throws Exception {

		try {
			test = reports.createTest("verifyMP");
			test.assignCategory("regression");

			driver = driverFact.getEventDriver(webdriver, test);
			
			//-----------------------------------------Login Flow-------------------------------------------//
			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.userName);
			verifyElementTextContains(login.userLabel, "username", test);
			verifyElementTextContains(login.passwordLabel, "password", test);
			verifyElementTextContains(login.forgotTxt, "Forgot username or password?", test);
			login.loginDB(USERNAME, PASSWORD);
			
			//---------------------------------------Dashboard Flow-----------------------------------------//
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(dashboard.DashboardTab);
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			// waitForElementToDisplay(dashboard.BenefitCommencementTile);
			verifyElementTextContains(dashboard.BenefitCommencementTile, "Benefit Commencement", test);
			waitForElementToDisplay(dashboard.MPEstimatorTile);
			verifyElementTextContains(dashboard.MPEstimatorTile, "Multi-Plan Estimator Tool", test);

			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);
			
			//---------------------------------------MultiPlan Flow-----------------------------------------//
			clickElement(dashboard.DashboardTab);
			
			waitForElementToDisplay(dashboard.MPEstimatorTile);
			verifyElementTextContains(dashboard.MPEstimatorTile, "Multi-Plan Estimator Tool", test);
			verifyElementTextContains(dashboard.MP_EstNowBtn, "ESTIMATE NOW", test);
			clickElement(dashboard.MP_EstNowBtn);

			clickElement(dashboard.PopOKBtn);
			
			//---------------------------------------Estimate  Flow-----------------------------------------//
			verifyElementTextContains(dashboard.EstYourMPPensionBenTab, "Estimate Your Multi-Plan Pension Benefits", test);
			waitForElementToDisplay(dashboard.EstYourMPPensionBenLabel);
			verifyElementTextContains(dashboard.EstYourMPPensionBenLabel, "Estimate Your Multi-Plan Pension Benefits", test);
			verifyElementIsPresent(dashboard.Est_Steps, test, "Estimate Steps");
			
			//------------------------------------Most Recent Tab Flow--------------------------------------//
			clickElement(dashboard.MstRcntMPEstTab);
			waitForElementToDisplay(dashboard.MPEstResultsLabel);
			verifyElementTextContains(dashboard.MPEstResultsLabel, "Multi-Plan Estimate Results", test);
			verifyElementIsPresent(dashboard.MP_CCDetails, test, "Multi-Plan Calc-Comparison Details");
			verifyElementIsPresent(dashboard.ViewAsmptnsBtn, test, "View Assumption Button");
			clickElement(dashboard.ViewAsmptnsBtn);
			waitForElementToDisplay(dashboard.AsmptnsDetails);
			verifyElementIsPresent(dashboard.AsmptnsDetails, test, "Assumption Details");
			clickElement(dashboard.AsmptnsClose);
			
			//------------------------------------Saved Estimates Flow---------------------------------------//
			clickElement(dashboard.SavedMPEstTab);
			waitForElementToDisplay(dashboard.SavedMPEstLabel);
			verifyElementTextContains(dashboard.SavedMPEstLabel, "Saved Multi-Plan Estimates", test);
			verifyElementIsPresent(dashboard.SavedMPDetails, test, "Saved Estimate Details");
			
			//-----------------------------------------Logout Flow-------------------------------------------//
			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyMP()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyMP()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}

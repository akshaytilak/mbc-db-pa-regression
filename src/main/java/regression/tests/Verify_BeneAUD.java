package regression.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.regression.MyAccountPage;
import pages.regression.DashboardPage;
import pages.regression.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_BeneAUD extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_BeneAUD(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_BeneAUD beneaud = new Verify_BeneAUD("BeneAUD");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true)
	public void verifyBeneAUD() throws Exception {

		try {
			test = reports.createTest("verifyBeneAUD");
			test.assignCategory("regression");

			driver = driverFact.getEventDriver(webdriver, test);
			
			//-------------------------------------------Login Flow----------------------------------------------//
			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.userName);
			verifyElementTextContains(login.userLabel, "username", test);
			verifyElementTextContains(login.passwordLabel, "password", test);
			verifyElementTextContains(login.forgotTxt, "Forgot username or password?", test);
			login.loginDB(USERNAME, PASSWORD);
			
			//-----------------------------------------Dashboard Flow--------------------------------------------//
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(dashboard.DashboardTab);
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			
			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);

			scrollToElement(driver, dashboard.MenuBtn);
			clickElement(dashboard.MenuBtn);
			
			//------------------------------------Home Link Verification-----------------------------------------//
			waitForElementToDisplay(dashboard.HomeBtn);
			waitForElementToDisplay(dashboard.MyAccountLabel);
			verifyElementTextContains(dashboard.MyAccountLabel, "My Account", test);
			verifyElementTextContains(dashboard.ProfileLink, "Profile", test);
			verifyElementTextContains(dashboard.CommunicationsLink, "Communication", test);
			verifyElementTextContains(dashboard.BeneficiariesLink, "Beneficiaries", test);
			
			clickElement(dashboard.BeneficiariesLink);
			
			//-----------------------------------------MyAccount Flow-------------------------------------------//
			MyAccountPage myacc = new MyAccountPage(driver);
			waitForElementToDisplay(myacc.MyAccountLabel);
			verifyElementIsPresent(myacc.MyAccountLabel, test, "My Account Label");
			waitForElementToDisplay(myacc.BeneficiariesLabel);
			verifyElementTextContains(myacc.BeneficiariesLabel, "Beneficiaries", test);
			verifyElementIsPresent(myacc.RetirementPensionLabel, test, "Retirement Pension Label");
			
			//--------------------------------------Add Beneficiary Flow----------------------------------------//
			myacc.editBeneficiaryStepZero(test);
			myacc.AddNewBeneficiary(driver, test);
			
			//-------------------------------------Edit Beneficiary Flow----------------------------------------//
			myacc.editBeneficiaryStepZero(test);
			myacc.UpdateBeneficiary(driver, test);
			
			//-----------------------------------Delete Beneficiary Flow----------------------------------------//
			myacc.editBeneficiaryStepZero(test);
			myacc.DeleteBeneficiary(test);
			
			//-----------------------------------------Logout Flow------------------------------------------//
			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyBeneAUD()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyBeneAUD()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}

package regression.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.regression.DashboardPage;
import pages.regression.EstimatePage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_BSHybrid extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_BSHybrid(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_BSHybrid bsh = new Verify_BSHybrid("Hybrid");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true, priority = 1)
	public void verifyBSH() throws Exception {

		try {

			test = reports.createTest("Verify_BSHybrid");
			test.assignCategory("regression");

			driver = driverFact.getEventDriver(webdriver, test);
			
			//---------------------------------------Dashboard Flow-----------------------------------------//
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(dashboard.DashboardTab, driver, 500);
			verifyElementTextContains(dashboard.BenefitCommencementTile, "Benefit Commencement", test);
			
			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);
			
			clickElement(dashboard.DashboardTab);
			
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			verifyElementTextContains(dashboard. HybPlantitle, "ADP Pension Retirement Plan", test);
			verifyElementTextContains(dashboard. APLabel, "Active Portion", test);
			verifyElementTextContains(dashboard.APBalanceLabel, "Balances", test);
			verifyElementIsPresent(dashboard.APAsoftext, test, " Active Portion As of Label");
			dashboard.verifycurrentbalance(test, dashboard.APcurrentbalanceamount, "Active Portion Balance");
			
			verifyElementTextContains(dashboard. FPLabel, "Frozen Portion", test);
			verifyElementIsPresent(dashboard.FPAsoftext, test, " Frozen Portion As of Label");
			dashboard.verifycurrentbalance(test, dashboard.FPcurrentbalanceamount, "Frozen Portion Balance");
			
			waitForElementToDisplay(dashboard.ImportantDatesButton);
			clickElement(dashboard.ImportantDatesButton);
			waitForElementToDisplay(dashboard.ImportantDatesLabel);
			verifyElementTextContains(dashboard.ImportantDatesLabel, "Important Dates", test);
			clickElement(dashboard.IDBackButton);
			verifyElementTextContains(dashboard.APBalanceLabel, "Balances", test);
			
			clickElement(dashboard.ViewEstimateLinkOnTile);
			
			//---------------------------------------Estimate Page Flow-----------------------------------------//
			EstimatePage Estimate = new EstimatePage(driver);
			Estimate.VerifyHybPlanSummary(test);
			
			clickElement(Estimate.HybHistoryLink);
			waitForElementToDisplay(Estimate.HybHistoryLabel);
			verifyElementTextContains(Estimate.HybHistoryLabel, "History", test);

			clickElementUsingJavaScript(driver, Estimate.EstimatesTab);
			waitForElementToDisplay(Estimate.EstimateYourPensionLink);
			verifyElementTextContains(Estimate.EstimateYourPensionLink, "Estimate Your Pension", test);
			waitForElementToDisplay(Estimate.EstimateYourPensionLabel);
			verifyElementTextContains(Estimate.EstimateYourPensionLabel, "Estimate Your Pension", test);

			clickElement(Estimate.SavedEstimatesLink);
			waitForElementToDisplay(Estimate.SavedEstimatesLabel);
			verifyElementTextContains(Estimate.SavedEstimatesLabel, "Saved Estimates", test);

			clickElement(Estimate.MyDataTab);
			waitForElementToDisplay(Estimate.MyDataLabel);
			verifyElementTextContains(Estimate.MyDataLabel, "My Data", test);
					
			//-----------------------------------------Logout Flow------------------------------------------//
			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyBSH()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyBSH()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}

package regression.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyElementIsPresent;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.regression.DashboardPage;
import pages.regression.InPayPage;
import pages.regression.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_PayDetails extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_PayDetails(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_PayDetails pdtl = new Verify_PayDetails("PDtl");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true)
	public void verifyPayDetails() throws Exception {

		try {
			test = reports.createTest("verifyPayDetails");
			test.assignCategory("regression");

			driver = driverFact.getEventDriver(webdriver, test);
			
			//-----------------------------------------Login Flow-------------------------------------------//
			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.userName);
			verifyElementTextContains(login.userLabel, "username", test);
			verifyElementTextContains(login.passwordLabel, "password", test);
			verifyElementTextContains(login.forgotTxt, "Forgot username or password?", test);
			login.loginDB(USERNAME, PASSWORD);
			
			//---------------------------------------Dashboard Flow-----------------------------------------//
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(dashboard.DashboardTab, driver, 400);
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			verifyElementTextContains(dashboard.RetirementPlanningTile, "RETIREMENT PLANNING", test);
			
			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);

			scrollToElement(driver, dashboard.MenuBtn);
			clickElement(dashboard.MenuBtn);
			
			//-----------------------------------Home Link Verification-------------------------------------//
			waitForElementToDisplay(dashboard.HomeBtn);
			waitForElementToDisplay(dashboard.MyAccountLabel);
			verifyElementTextContains(dashboard.MyAccountLabel, "My Account", test);
			verifyElementTextContains(dashboard.Forms_DocLabel, "Forms & Documents", test);
			verifyElementTextContains(dashboard.KnowledgeCenterLabel, "Knowledge Center", test);
			verifyElementTextContains(dashboard.UploadDocsLabel, "UPLOAD DOCUMENTS", test);
			
			clickElement(dashboard.OverviewLink_hm);
			
			//-----------------------------------------Inpay Flow-------------------------------------------//
			InPayPage Inpay = new InPayPage(driver);
			waitForElementToDisplay(Inpay.PaymentSummaryLabel);
			verifyElementTextContains(Inpay.PaymentSummaryLabel, "Payment Summary", test);
			clickElement(Inpay.PaymentHistoryLink);
			waitForElementToDisplay(Inpay.PaymentHistoryLabel);
			verifyElementTextContains(Inpay.PaymentHistoryLabel, "Payment History", test);
			clickElement(Inpay.ScheduledDeductionsLink);
			waitForElementToDisplay(Inpay.ScheduledDeductionsLabel);
			verifyElementTextContains(Inpay.ScheduledDeductionsLabel, "Scheduled Deductions", test);
			
			clickElement(Inpay.PaymentHistoryLink);
			waitForElementToDisplay(Inpay.PaymentHistoryLabel);
			verifyElementTextContains(Inpay.PaymentHistoryLabel, "Payment History", test);
			
			//---------------------------------------Pay Detail Flow----------------------------------------//
			Inpay.check_WTDH(test);
			clickElement(Inpay.PaymentDetailsLinks);
			waitForElementToDisplay(Inpay.PaymentDetailsLabel);
			verifyElementTextContains(Inpay.PaymentDetailsLabel, "Payment Details", test);
			verifyElementIsPresent(Inpay.PaymentDetails, test, "Payment Details");
			
			//-----------------------------------------Logout Flow------------------------------------------//
			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("VerifyPayDetails()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("VerifyPayDetails()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}

package regression.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.hoverAndClickOnElement;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.regression.DashboardPage;
import pages.regression.EstimatePage;
import pages.regression.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_DBCB extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_DBCB(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_DBCB dbcb = new Verify_DBCB("CB");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true, priority = 1)
	public void verifyDBCB() throws Exception {

		try {

			test = reports.createTest("Verify_DBCB");
			test.assignCategory("regression");

			driver = driverFact.getEventDriver(webdriver, test);
			
			//-----------------------------------------Login Flow-------------------------------------------//
			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.BWuserName);
			verifyElementTextContains(login.BWLogLabel, "Log in to your existing account.", test);
			verifyElementTextContains(login.BWforgotTxt, "Forgot User Name or Password?", test);
			login.BWloginDB(USERNAME, PASSWORD);
			
			//---------------------------------------Dashboard Flow-----------------------------------------//
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(dashboard.DashboardTab, driver, 500);
			verifyElementTextContains(dashboard.ADPTile, "ADP Voluntary Early Retirement Program", test);
			verifyElementTextContains(dashboard.BenefitCommencementTile, "Benefit Commencement", test);
			
			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);
			
			clickElement(dashboard.DashboardTab);
			
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			verifyElementTextContains(dashboard.AVONPlantitle, "MBC Test Desc1 - Personal Retirement Account Plan", test);
			verifyElementIsPresent(dashboard.Asoftext, test, "As of Label");
			verifyElementTextContains(dashboard.BalanceLabel, "Balances", test);
			dashboard.verifycurrentbalance(test, dashboard.currentbalanceamount, "Balance Amount");
			verifyElementTextContains(dashboard.xaxisgraph, "2004", test);
			verifyElementTextContains(dashboard.yaxisgraph, "$4K", test);
			verifyElementTextContains(dashboard.CBbeginningbalance, "Beginning Balance", test);
			verifyElementTextContains(dashboard.CBcontribution, "Contribution", test);
			verifyElementTextContains(dashboard.CBInterest, "Interest", test);
			verifyElementTextContains(dashboard.CBadjustment, "Adjustment", test);
			verifyElementIsPresent(dashboard.CBbeginningbalancecolor, test, "Beginning Balance Color");
			verifyElementIsPresent(dashboard.CBcontributioncolor, test, "Contribution Color");
			verifyElementIsPresent(dashboard.CBInterestcolor, test, "Interest Color");
			verifyElementIsPresent(dashboard.CBadjustmentcolor, test, "Adjustment Color");
			
			dashboard.verifyGraphPopup(test, "Dashboard");
			
			waitForElementToDisplay(dashboard.ImportantDatesButton);
			clickElement(dashboard.ImportantDatesButton);
			waitForElementToDisplay(dashboard.ImportantDatesLabel);
			verifyElementTextContains(dashboard.ImportantDatesLabel, "Important Dates", test);
			clickElement(dashboard.IDBackButton);
			verifyElementTextContains(dashboard.BalanceLabel, "Balances", test);
			
			clickElement(dashboard.ViewEstimateLinkOnTile);
			
			//-----------------------------------Home Link Verification-------------------------------------//
			EstimatePage Estimate = new EstimatePage(driver);
			waitForElementToDisplay(Estimate.YourPlanSummaryLabel);
			verifyElementTextContains(Estimate.YourPlanSummaryLabel, "Your Plan Summary", test);
			
			scrollToElement(driver, dashboard.MenuBtn);
			clickElement(dashboard.MenuBtn);

			waitForElementToDisplay(dashboard.HomeBtn);
			waitForElementToDisplay(dashboard.MyAccountLabel);
			verifyElementTextContains(dashboard.MyAccountLabel, "My Account", test);
			verifyElementTextContains(dashboard.Forms_DocLabel, "Forms & Documents", test);
			verifyElementTextContains(dashboard.KnowledgeCenterLabel, "Knowledge Center", test);
						
			clickElement(dashboard.HomeBtn);
			waitForElementToDisplay(dashboard.DashboardTab);
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			
			//-----------------------------------------Logout Flow------------------------------------------//
			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyDBCB()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyDBCB()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}

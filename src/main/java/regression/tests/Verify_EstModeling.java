package regression.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementIsNotPresent;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.regression.DashboardPage;
import pages.regression.EstimatePage;
import pages.regression.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_EstModeling extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_EstModeling(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_EstModeling cmpcal = new Verify_EstModeling("EstMod");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true)
	public void verifyEstMod() throws Exception {

		try {
			test = reports.createTest("verifyEstMod");
			test.assignCategory("regression");

			driver = driverFact.getEventDriver(webdriver, test);
			
			//-----------------------------------------Login Flow-------------------------------------------//
			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.userName);
			verifyElementTextContains(login.userLabel, "username", test);
			verifyElementTextContains(login.passwordLabel, "password", test);
			verifyElementTextContains(login.forgotTxt, "Forgot username or password?", test);
			login.loginDB(USERNAME, PASSWORD);
			
			//---------------------------------------Dashboard Flow-----------------------------------------//
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(dashboard.DashboardTab);
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			verifyElementTextContains(dashboard.RetirementPlanningTile, "RETIREMENT PLANNING", test);
			
			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);

			scrollToElement(driver, dashboard.MenuBtn);
			clickElement(dashboard.MenuBtn);
			
			//-----------------------------------Home Link Verification-------------------------------------//
			waitForElementToDisplay(dashboard.HomeBtn);
			waitForElementToDisplay(dashboard.MyAccountLabel);
			verifyElementTextContains(dashboard.MyAccountLabel, "My Account", test);
			verifyElementTextContains(dashboard.ProfileLink, "Profile", test);
			verifyElementTextContains(dashboard.CommunicationsLink, "Communication", test);
			verifyElementTextContains(dashboard.BeneficiariesLink, "Beneficiaries", test);
	
			//clickElement(dashboard.KelloggHeritageLink);
			clickElement(dashboard.OverviewLink_hm);
			
			//---------------------------------------Estimate Page Flow-----------------------------------------//
			EstimatePage Estimate = new EstimatePage(driver);
			waitForElementToDisplay(Estimate.YourPlanSummaryLabel);
			verifyElementTextContains(Estimate.YourPlanSummaryLabel, "Your Plan Summary", test);

			clickElementUsingJavaScript(driver, Estimate.EstimatesTab);
			waitForElementToDisplay(Estimate.EstimateYourPensionLink);
			verifyElementTextContains(Estimate.EstimateYourPensionLink, "Estimate Your Pension", test);
			waitForElementToDisplay(Estimate.EstimateYourPensionLabel);
			verifyElementTextContains(Estimate.EstimateYourPensionLabel, "Estimate Your Pension", test);

			clickElement(Estimate.SavedEstimatesLink);
			waitForElementToDisplay(Estimate.SavedEstimatesLabel);
			verifyElementTextContains(Estimate.SavedEstimatesLabel, "Saved Estimates", test);

			clickElement(Estimate.MyDataTab);
			waitForElementToDisplay(Estimate.MyDataLabel);
			verifyElementTextContains(Estimate.MyDataLabel, "My Data", test);

			//clickElement(Estimate.MyElectionsTab);
			//waitForElementToDisplay(Estimate.MyElectionsLabel);
			//verifyElementTextContains(Estimate.MyElectionsLabel, "My Elections", test);
			
			clickElementUsingJavaScript(driver, Estimate.EstimatesTab);
			waitForElementToDisplay(Estimate.EstimateYourPensionLink);
			verifyElementTextContains(Estimate.EstimateYourPensionLink, "Estimate Your Pension", test);
			clickElement(Estimate.MostRecentEstimateLink);
			waitForElementToDisplay(Estimate.EstimateDetailsLabel);
			verifyElementTextContains(Estimate.EstimateDetailsLabel, "Estimate Details", test);
			
			//--------------------------------------Estimate Modeling Flow--------------------------------------//
			Estimate.EstimateModeling(driver, test);
						
			//-----------------------------------------Logout Flow------------------------------------------//
			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyEstMod()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyEstMod()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}

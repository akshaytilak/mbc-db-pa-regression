package regression.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.regression.BankPage;
import pages.regression.DashboardPage;
import pages.regression.InPayPage;
import pages.regression.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_BAUD extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_BAUD(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_BAUD baud = new Verify_BAUD("BAUD");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true)
	public void verifyBAUD() throws Exception {

		try {
			test = reports.createTest("verifyBAUD");
			test.assignCategory("regression");

			driver = driverFact.getEventDriver(webdriver, test);
			
			//-----------------------------------------Login Flow-------------------------------------------//
			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.userName);
			verifyElementTextContains(login.userLabel, "username", test);
			verifyElementTextContains(login.passwordLabel, "password", test);
			verifyElementTextContains(login.forgotTxt, "Forgot username or password?", test);
			login.loginDB(USERNAME, PASSWORD);
			
			//---------------------------------------Dashboard Flow-----------------------------------------//
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(dashboard.DashboardTab);
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			verifyElementTextContains(dashboard.RetirementPlanningTile, "RETIREMENT PLANNING", test);
			
			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);

			scrollToElement(driver, dashboard.MenuBtn);
			clickElement(dashboard.MenuBtn);
			
			//-----------------------------------Home Link Verification-------------------------------------//
			waitForElementToDisplay(dashboard.HomeBtn);
			waitForElementToDisplay(dashboard.MyAccountLabel);
			verifyElementTextContains(dashboard.MyAccountLabel, "My Account", test);
			verifyElementTextContains(dashboard.ProfileLink, "Profile", test);
			verifyElementTextContains(dashboard.CommunicationsLink, "Communication", test);
			verifyElementTextContains(dashboard.BeneficiariesLink, "Beneficiaries", test);
			verifyElementTextContains(dashboard.Forms_DocLabel, "Forms & Documents", test);
			verifyElementTextContains(dashboard.Retirement_PensionLink, "Retirement Pension", test);
			verifyElementTextContains(dashboard.KnowledgeCenterLabel, "Knowledge Center", test);
			verifyElementTextContains(dashboard.RetirementLink, "Retirement", test);
			verifyElementTextContains(dashboard.MoneyManagementLink, "Money Management", test);
			verifyElementTextContains(dashboard.UploadDocsLabel, "UPLOAD DOCUMENTS", test);
			verifyElementTextContains(dashboard.GeneralDocumentsLink, "General Documents", test);
			verifyElementTextContains(dashboard.TaxWithHoldinglink_hm, "Tax Withholding", test);
			verifyElementTextContains(dashboard.PaymentMethodlink_hm, "Payment Method", test);
			
			clickElement(dashboard.OverviewLink_hm);
			
			//-----------------------------------------Inpay Flow-------------------------------------------//
			InPayPage Inpayop = new InPayPage(driver);
			waitForElementToDisplay(Inpayop.PaymentSummaryLabel);
			verifyElementTextContains(Inpayop.PaymentSummaryLabel, "Payment Summary", test);
			clickElement(Inpayop.PaymentHistoryLink);
			waitForElementToDisplay(Inpayop.PaymentHistoryLabel);
			verifyElementTextContains(Inpayop.PaymentHistoryLabel, "Payment History", test);
			clickElement(Inpayop.ScheduledDeductionsLink);
			waitForElementToDisplay(Inpayop.ScheduledDeductionsLabel);
			verifyElementTextContains(Inpayop.ScheduledDeductionsLabel, "Scheduled Deductions", test);
			
			clickElement(Inpayop.TaxWithHoldingTab);
			waitForElementToDisplay(Inpayop.FederalTaxWithHoldingLabel);
			verifyElementTextContains(Inpayop.FederalTaxWithHoldingLabel, "Federal Tax Withholding", test);
			clickElement(Inpayop.StateLink);
			waitForElementToDisplay(Inpayop.StateTaxWithHoldingLabel);
			verifyElementTextContains(Inpayop.StateTaxWithHoldingLabel, "State Tax Withholding", test);
			
			//----------------------------------Edit Payment Method Flow------------------------------------//
			clickElement(Inpayop.PaymentMethodTab);
			waitForElementToDisplay(Inpayop.PaymentMethodLabel);
			verifyElementTextContains(Inpayop.PaymentMethodLabel, "Payment Method", test);
			
			BankPage banklmnt = new BankPage(driver);
			clickElement(banklmnt.PaymentMethodEdit);
			waitForElementToDisplay(banklmnt.EditPaymentMethodHeader);
			verifyElementTextContains(banklmnt.EditPaymentMethodHeader, "Edit Payment Method", test);
			banklmnt.editPaymentMethod(driver);
			waitForElementToDisplay(banklmnt.SuccessfulPMEditMsg);
			verifyElementTextContains(banklmnt.SuccessfulPMEditMsg, "Your changes have not been saved.", test);
			
			//-----------------------------------Add Bank Account Flow--------------------------------------//
			waitForElementToDisplay(banklmnt.PaymentMethodEdit);
			clickElement(banklmnt.PaymentMethodEdit);
			waitForElementToDisplay(banklmnt.EditPaymentMethodHeader);
			verifyElementTextContains(banklmnt.EditPaymentMethodHeader, "Edit Payment Method", test);
			waitForElementToDisplay(banklmnt.AddBankAccountButton);
			clickElement(banklmnt.AddBankAccountButton);
			waitForElementToDisplay(banklmnt.AddBankAccountHeader);
			verifyElementTextContains(banklmnt.AddBankAccountHeader, "Add a Bank Account", test);	
			banklmnt.addBankAccount();
			waitForElementToDisplay(banklmnt.EditPaymentMethodHeader);
			verifyElementTextContains(banklmnt.EditPaymentMethodHeader, "Edit Payment Method", test);
			
			//------------------------------------Edit Bank Account Flow------------------------------------//
			clickElement(banklmnt.BankAccountslink);
			waitForElementToDisplay(banklmnt.BankAccountHeader);
			verifyElementTextContains(banklmnt.BankAccountHeader, "Bank Accounts", test);
			banklmnt.editBankAccount();
			waitForElementToDisplay(banklmnt.SuccessfulEditMsg);
			verifyElementTextContains(banklmnt.SuccessfulEditMsg, "Your changes have been saved.", test);
			
			//-----------------------------------Delete Bank Account Flow-----------------------------------//
			waitForElementToDisplay(banklmnt.BankAccountHeader);
			verifyElementTextContains(banklmnt.BankAccountHeader, "Bank Accounts", test);
			banklmnt.deleteBankAccount();
			waitForElementToDisplay(banklmnt.SuccessfulDeleteMsg);
			verifyElementTextContains(banklmnt.SuccessfulDeleteMsg, 
					"You have successfully deleted an unassigned bank account.", test);
			
			//-----------------------------------------Logout Flow------------------------------------------//
			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyBAUD()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyBAUD()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}

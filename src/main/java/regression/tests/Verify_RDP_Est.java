package regression.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.regression.DashboardPage;
import pages.regression.EstimatePage;
import pages.regression.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_RDP_Est extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver=null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public Verify_RDP_Est(String appName) {
		super(appName);
	}
	
	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_RDP_Est rdpEst = new Verify_RDP_Est("RDP");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled=true, priority = 1)
	public void verify_RDP_Est() throws Exception {
		
		try {
			test = reports.createTest("verify_RDP_Est");
			test.assignCategory("regression");
			
			driver = driverFact.getEventDriver(webdriver, test); 
			
			//-----------------------------------------Login Flow-------------------------------------------//
			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.userName);
			verifyElementTextContains(login.userLabel, "username", test);
			verifyElementTextContains(login.passwordLabel, "password", test);
			verifyElementTextContains(login.forgotTxt, "Forgot username or password?", test);
			login.loginDB(USERNAME, PASSWORD);
			
			//---------------------------------------Dashboard Flow-----------------------------------------//			
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(dashboard.RetirementPensionTitle, driver, 500);
			verifyElementTextContains(dashboard.RetirementPlanningTile,"RETIREMENT PLANNING",test);
			
			scrollToElement(driver, dashboard.MenuBtn);
			clickElement(dashboard.MenuBtn);
			
			//-----------------------------------Home Link Verification-------------------------------------//
			waitForElementToDisplay(dashboard.HomeBtn);
			waitForElementToDisplay(dashboard.MyAccountLabel);
			verifyElementTextContains(dashboard.MyAccountLabel, "My Account", test);
			verifyElementTextContains(dashboard.Forms_DocLabel, "Forms & Documents", test);
			verifyElementTextContains(dashboard.KnowledgeCenterLabel, "Knowledge Center", test);
			verifyElementTextContains(dashboard.UploadDocsLabel, "UPLOAD DOCUMENTS", test);
			verifyElementTextContains(dashboard.CovestroPensionPlanHeader,"Covestro Pension Plan",test);
			verifyElementTextContains(dashboard.OverviewLink_hm,"Overview",test);
			clickElement(dashboard.OverviewLink_hm);
			
			//---------------------------------------Estimate Page Flow-----------------------------------------//
			EstimatePage Estimate = new EstimatePage(driver);
			waitForElementToDisplay(Estimate.YourPlanSummaryLabel);
			verifyElementTextContains(Estimate.YourPlanSummaryLabel,"Your Plan Summary",test);
			
			clickElementUsingJavaScript(driver, Estimate.EstimatesTab);
			waitForElementToDisplay(Estimate.EstimateYourPensionLabel);
			verifyElementTextContains(Estimate.EstimateYourPensionLabel, "Estimate Your Pension", test);
			waitForElementToDisplay(Estimate.NormalRetirementLabel);
			verifyElementTextContains(Estimate.NormalRetirementLabel, "Normal Retirement", test);
			waitForElementToDisplay(Estimate.Normalretirementradiobutton);
			
			//--------------------------------------Estimate Request Flow--------------------------------------//
		    Estimate.Estimatecalc(test, "RDP");
		    
		    //-------------------------------------------Logout Flow------------------------------------------//
		    verifyElementTextContains(dashboard.logoutBtn,"Logout",test);
			clickElement(dashboard.logoutBtn);
			waitForElementToDisplay(login.userLabel);
			verifyElementTextContains(login.userLabel,"username",test);
			
		} catch (Error e) {
			e.printStackTrace();     
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verify_RDP_Est()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verify_RDP_Est()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.close();

		}
	} 

}


package pages.regression;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

public class MyAccountPage {
	WebDriver driver;

	public MyAccountPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	//------------------------------------------------------MyAccount   Elements------------------------------------------------------//
		
	@FindBy(xpath = "//h2[contains(text(),'My Account')]")
	public WebElement MyAccountLabel;
	
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(1) > a")
	public WebElement ProfileTab;
	
	//a/span[contains(text(),'Beneficiaries')]
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(2) > a")
	public WebElement BeneficiaryTab;
	
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(3) > a")
	public WebElement CommunicationTab;
	
	//------------------------------------------------------Beneficiary Elements------------------------------------------------------//
	
	@FindBy(xpath = "//h1[contains(text(),'Beneficiaries')]")
	public WebElement BeneficiariesLabel;
		
	@FindBy(xpath = "//h2[contains(text(),'Retirement Pension')]")
	public WebElement RetirementPensionLabel;
	
	@FindBy(xpath = "//span[@class='ng-binding'][contains(text(),'Edit Beneficiaries')]")
	public WebElement EditBeneButton;
	
	@FindBy(xpath = "//*[contains(text(),'Online Beneficiary Signature')]")
	public WebElement BeneDisclaimerHeader;
	
	@FindBy(xpath = "(//button[@type='button'][contains(text(),'No')])[1]")
	public WebElement BeneNoRadioButton;
	
	@FindBy(xpath = "//div[@class='seperated']/button")
	public WebElement BeneDisclaimerCheckBox;
	
	@FindBy(xpath = "//button[contains(text(),'Next')]")
	public WebElement NextDisclaimerButton;

	@FindBy(xpath = "//span[contains(text(),'Edit Beneficiaries')]")
	public WebElement EditBenePageHeader;
	
	@FindBy(xpath = "//span[contains(text(),'Add a Beneficiary')]")
	public WebElement AddBeneButton;
	
	@FindBy(xpath = "//h4[contains(text(),'Add a Beneficiary')]")
	public WebElement AddBeneLabel;
	
	@FindBy(xpath = "(//button[@title='Nothing selected'])[1]")
	public WebElement BeneTypeDropdown;
	
	@FindBy(xpath = "//span[contains(text(),'Add a New Trust')]")
	public WebElement TypeDropdownOption1;
	
	@FindBy(xpath = "//span[contains(text(),'Add a New Individual')]")
	public WebElement TypeDropdownOption2;
	
	@FindBy(xpath = "//button[@title='Nothing selected']")
	public WebElement BenePriorityDropdown;
	
	@FindBy(xpath = "//li/a/span[contains(text(),'Primary')]")
	public WebElement PriorityOption_Primary;
	
	@FindBy(xpath = "//li/a/span[contains(text(),'Secondary')]")
	public WebElement PriorityOption_Secondary;
	
	@FindBy(xpath = "//button[contains(text(),'Add to List')]")
	public WebElement AddTolistButton;
	
	@FindBy(xpath = "//h4[contains(text(),'Add a New Individual')]")
	public WebElement BeneFormHeader;
	
	@FindBy(xpath = "//input[@name='BeneficiarySsn'][@aria-invalid='true']")
	public WebElement BeneSSNInput;
	
	@FindBy(xpath = "//input[@name='BirthDate'][@aria-invalid='true']")
	public WebElement  BeneDOBInput;
	
	@FindBy(xpath = "//button[@tabindex='3'][@title='Nothing selected']")
	public WebElement RelationDropdown;
	
	public WebElement RelationshipDropdownOption(WebDriver driver, String Option) {
		WebElement RelationshipDropdownOption = driver.findElement(By.xpath("//button[@tabindex='3'][@title='Nothing selected']//following::div/ul/li/a/span[contains(text(),'"+ Option +"')]"));
		return RelationshipDropdownOption;
	}
	
	@FindBy(xpath = "//input[@name='Name'][@aria-invalid='true']")
	public WebElement BeneNameInput;
	
	@FindBy(xpath = "//input[@name='LastName'][@aria-invalid='true']")
	public WebElement BeneLastNameInput;
	
	public WebElement GenderOption(WebDriver driver, String Option) {
		WebElement CountryDropdownOption = driver.findElement(By.xpath("//button[@name='Gender'][@aria-invalid='true'][contains(text(),'"+ Option +"')]"));
		return CountryDropdownOption;
	}
	
	@FindBy(xpath = "//input[@name='Address1'][@aria-invalid='true']")
	public WebElement Address1;
	
	@FindBy(xpath = "//input[@name='City'][@aria-invalid='true']")
	public WebElement City;
	
	@FindBy(xpath = "//button[@tabindex='12'][@title='Nothing selected']")
	public WebElement CountryDropdown;
	
	public WebElement CountryDropdownOption(WebDriver driver, String Option) {
		WebElement CountryDropdownOption = driver.findElement(By.xpath("//button[@tabindex='12'][@title='Nothing selected']//following::div/ul/li/a/span[contains(text(),'"+ Option +"')]"));
		return CountryDropdownOption;
	}
	
	@FindBy(xpath = "//button[@tabindex='14'][@title='Nothing selected']")
	public WebElement StateDropdown;
	
	public WebElement StateDropdownOption(WebDriver driver, String Option) {
		WebElement StateDropdownOption = driver.findElement(By.xpath("//button[@tabindex='14'][@title='Nothing selected']//following::div/ul/li/a/span[contains(text(),'"+ Option +"')]"));
		return StateDropdownOption;
	}
	
	@FindBy(xpath = "//input[@class='zip-code-js ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required ng-valid-pattern form-control']")
	public WebElement ZipCode;
	
	@FindBy(xpath = "//div[@class='form-btns']/button")
	public WebElement BeneFormAddTolist;
	
	@FindBy(xpath = "//input[@name='Percent'][@aria-invalid='true']")
	public WebElement BenePercent; 
	
	@FindBy(xpath = "//button[contains(text(),'Save')]")
	public WebElement BeneSave;
	
	@FindBy(xpath = "//button[contains(text(),'Cancel')]")
	public WebElement BeneCancel;
	
	@FindBy(xpath = "//div[@class='confirmation']")
	public WebElement ConfirmationMsg;
	
	@FindBy(xpath = "//p[contains(text(),'Primary')]")
	public WebElement PrimaryHeader;
	
	@FindBy(xpath = "//div[@class='benes-detail-action']/button/i[@aria-label='Click to expand']")
	public WebElement BeneExpandButton;
	
	public WebElement BeneDoB(WebDriver driver, String Date) {
		WebElement BeneDob = driver.findElement(By.xpath("//p[@ng-if='beneficiary.BirthDate'][contains(text(),'"+ Date +"')]"));
		return BeneDob;
	}
	
	@FindBy(xpath = "(//span[contains(text(),'Remove')])[1]")
	public WebElement RemoveBeneLink;
	
	@FindBy(xpath = "//span[contains(text(),'You have no beneficiaries on file.')]")
	public WebElement NoBeneficiaryMsg;
	
	//i[@aria-label='Click to collapse']
	
	//------------------------------------------------------Beneficiary  Methods------------------------------------------------------//
		
	public void editBeneficiaryStepZero(ExtentTest test) throws InterruptedException {
		waitForElementToDisplay(EditBeneButton);
		clickElement(EditBeneButton);
		waitForElementToDisplay(BeneDisclaimerHeader);
		verifyElementIsPresent(BeneDisclaimerHeader, test, "Disclaimer Header");
		clickElement(BeneNoRadioButton);
		waitForElementToDisplay(BeneDisclaimerCheckBox);
		clickElement(BeneDisclaimerCheckBox);
		waitForElementToDisplay(NextDisclaimerButton);
		clickElement(NextDisclaimerButton);
		waitForElementToDisplay(EditBenePageHeader);
		verifyElementIsPresent(EditBenePageHeader, test, "Edit Page Header");
	}
	
	//-----------------------------------------------------Beneficiary Add Method-----------------------------------------------------//
	public void AddNewBeneficiary(WebDriver driver, ExtentTest test) throws InterruptedException {
		waitForElementToDisplay(AddBeneButton);
		clickElement(AddBeneButton);
		waitForElementToDisplay(AddBeneLabel);
		verifyElementIsPresent(AddBeneLabel, test, "Add Beneficiary Label");
		clickElement(BeneTypeDropdown);
		waitForElementToDisplay(TypeDropdownOption2);
		clickElement(TypeDropdownOption2);
		waitForElementToDisplay(BenePriorityDropdown);
		clickElement(BenePriorityDropdown);
		waitForElementToDisplay(PriorityOption_Primary);
		clickElement(PriorityOption_Primary);
		waitForElementToDisplay(AddTolistButton);
		clickElement(AddTolistButton);
		waitForElementToDisplay(BeneFormHeader);
		verifyElementIsPresent(BeneFormHeader, test, "Add Beneficiary Label");
		setInput(BeneSSNInput, InPayPage.randnum(999999999, 100000000));
		BeneDOBInput.sendKeys(ROL_Page.CurDateManipulator(1,2,-30));
		waitForElementToDisplay(RelationDropdown);
		clickElement(RelationDropdown);
		clickElement(RelationshipDropdownOption(driver,"Friend"));
		setInput(BeneNameInput, "Dan");
		setInput(BeneLastNameInput,"Test");
		clickElement(GenderOption(driver,"Male"));
		setInput(Address1, "Add1");
		setInput(City, "NYC");
		clickElement(CountryDropdown);
		clickElement(CountryDropdownOption(driver,"United States"));
		waitForElementToDisplay(StateDropdown);
		clickElement(StateDropdown);
		clickElement(StateDropdownOption(driver,"New York"));
		ZipCode.sendKeys("10001");
		waitForElementToDisplay(BeneFormAddTolist);
		clickElement(BeneFormAddTolist);
		waitForElementToDisplay(BenePercent);
		setInput(BenePercent, "100");
		waitForElementToDisplay(BeneSave);
		clickElement(BeneSave);
		//waitForElementToDisplay(ConfirmationMsg);
		//verifyElementIsPresent(ConfirmationMsg, test, "Confirmation Message");
		waitForElementToDisplay(BeneExpandButton);
		clickElement(BeneExpandButton);
		verifyElementTextContains(BeneDoB(driver, ROL_Page.CurDateManipulator(1,2,-30)), ROL_Page.CurDateManipulator(1,2,-30), test);
		verifyElementTextContains(driver.findElement(By.xpath("//p[contains(text(),'Male')]")), "Male", test);
		verifyElementTextContains(driver.findElement(By.xpath("//p[contains(text(),'Friend')]")), "Friend", test);
	}
	
	//----------------------------------------------------Beneficiary Update Method---------------------------------------------------//
	
	@FindBy(xpath = "(//button[@tabindex='3'])[1]")
	public WebElement UpdRelationDropdown;
	
	public WebElement UpdRelationshipDropdownOption(WebDriver driver, String Option) {
		WebElement RelationshipDropdownOption = driver.findElement(By.xpath("//button[@tabindex='3']//following::div/ul/li/a/span[contains(text(),'"+ Option +"')]"));
		return RelationshipDropdownOption;
	}
	
	@FindBy(xpath = "//input[@name='Name']")
	public WebElement UpdBeneNameInput;
	
	@FindBy(xpath = "//input[@name='LastName']")
	public WebElement UpdBeneLastNameInput;
	
	public WebElement UpdGenderOption(WebDriver driver, String Option) {
		WebElement CountryDropdownOption = driver.findElement(By.xpath("//button[@name='Gender'][contains(text(),'"+ Option +"')]"));
		return CountryDropdownOption;
	}
	public void UpdateBeneficiary(WebDriver driver, ExtentTest test) throws InterruptedException {
		waitForElementToDisplay(BeneExpandButton);
		clickElement(BeneExpandButton);
		waitForElementToDisplay(UpdRelationDropdown);
		clickElement(UpdRelationDropdown);
		clickElement(UpdRelationshipDropdownOption(driver,"Other"));
		setInput(UpdBeneNameInput, "DanUpd");
		setInput(UpdBeneLastNameInput,"TestUpd");
		clickElement(UpdGenderOption(driver,"Female"));
		waitForElementToDisplay(BeneSave);
		clickElement(BeneSave);
		waitForElementToDisplay(ConfirmationMsg);
		verifyElementIsPresent(ConfirmationMsg, test, "Confirmation Message");
		clickElement(BeneExpandButton);
		verifyElementTextContains(driver.findElement(By.xpath("//p[contains(text(),'Female')]")), "Female", test);
		verifyElementTextContains(driver.findElement(By.xpath("//p[contains(text(),'Other')]")), "Other", test);
	}
	
	//----------------------------------------------------Beneficiary Delete Method---------------------------------------------------//
	
	public void DeleteBeneficiary(ExtentTest test) throws InterruptedException {
		waitForElementToDisplay(RemoveBeneLink);
		clickElement(RemoveBeneLink);
		waitForElementToDisplay(BeneSave);
		clickElement(BeneSave);
		waitForElementToDisplay(NoBeneficiaryMsg);
		verifyElementIsPresent(ConfirmationMsg, test, "Confirmation Message");
		verifyElementTextContains(NoBeneficiaryMsg,  "You have no beneficiaries on file.", test);
	}
}

package pages.regression;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	WebDriver driver;

	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	// QAI/QAF/CIT/CSO/PROD
	@FindBy(id = "username")
	public WebElement userName;

	@FindBy(xpath = "//label[contains(text(),'Username')]")
	public WebElement userLabel;

	@FindBy(id = "password")
	public WebElement passWord;

	@FindBy(xpath = "//label[contains(text(),'Password')]")
	public WebElement passwordLabel;

	@FindBy(xpath = "//button[contains(text(),'Login')]")
	public WebElement loginBtn;

	@FindBy(xpath = "//p[contains(text(),'Forgot')]")
	public WebElement forgotTxt;

	public void loginDB(String username, String password) throws InterruptedException {
		setInput(userName, username);
		waitForElementToDisplay(passWord);
		setInput(passWord, password);
		clickElement(loginBtn);
	}

	// BWQAI
	@FindBy(xpath = "//span[contains(text(),'Log in to your existing account.')]")
	public WebElement BWLogLabel;
	
	@FindBy(id = "usernameId")
	public WebElement BWuserName;

	@FindBy(id = "passwordId")
	public WebElement BWpassWord;

	@FindBy(xpath = "//input[@type = 'submit']")
	public WebElement BWsubmitBtn;

	@FindBy(xpath = "//p[contains(text(),'Forgot')]")
	public WebElement BWforgotTxt;

	public void BWloginDB(String username, String password) throws InterruptedException {
		setInput(BWuserName, username);
		waitForElementToDisplay(BWpassWord);
		setInput(BWpassWord, password);
		clickElement(BWsubmitBtn);
	}
}

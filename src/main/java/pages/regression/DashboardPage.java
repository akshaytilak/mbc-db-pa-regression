package pages.regression;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.hoverAndClickOnElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class DashboardPage {
	WebDriver driver;

	public DashboardPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	//------------------------------------------General Elements------------------------------------------//
	
	// @FindBy(xpath = "//span[contains(text(),'Dashboard')]")
	@FindBy(css = "span[data-bo-bind*='Dashboard']")
	public WebElement DashboardTab;

	// @FindBy(xpath = "//span[contains(text(),'Benefits Summary')]")
	@FindBy(css = "span[data-bo-bind*='BenefitsSummary']")
	public WebElement BenefitsSummaryTab;

	@FindBy(xpath = "//span[contains(text(),'RETIREMENT PENSION')]")
	public WebElement RetirementPensionTitle;

	// @FindBy(xpath = "//span[contains(text(),'Benefit Commencement')]")
	@FindBy(css = "span[data-bo-bind*='BenefitCommencement']")
	public WebElement BenefitCommencementTile;
	
	@FindBy(xpath = "//span[contains(text(),'RETIREMENT PLANNING')]")
	public WebElement RetirementPlanningTile;

	@FindBy(css = "a[title='Estimate now']")
	public WebElement EstimateNowBtn;

	@FindBy(css = "span[mbc-content-i*='RetirementTitle']")
	public WebElement RetirementLabel;

	// @FindBy(xpath = "//span[contains(text(),'Menu')]")
	@FindBy(css = "span[data-bo-bind*='Menu']")
	public WebElement MenuBtn;

	// @FindBy(css = "a[href*='dashboard']")
	@FindBy(xpath = "//span[contains(text(),'Home')]")
	public WebElement HomeBtn;

	@FindBy(css = "a[href*='my-account/personal-information']")
	public WebElement EditProfileLink;

	@FindBy(xpath = "//h6[contains(text(),'My Account')]")
	public WebElement MyAccountLabel;

	@FindBy(xpath = "//span[contains(text(),'Profile')]")
	public WebElement ProfileLink;

	@FindBy(xpath = "//span[contains(text(),'Communications')]")
	public WebElement CommunicationsLink;

	@FindBy(xpath = "//span[contains(text(),'Beneficiaries')]")
	public WebElement BeneficiariesLink;
	
	@FindBy(xpath = "//h6[contains(text(),'Retirement Pension')]")
	public WebElement RetirementPensionLabel;
	
	@FindBy(xpath = "//h6[contains(text(),'Forms & Documents')]")
	public WebElement Forms_DocLabel;

	@FindBy(xpath = "//span[contains(text(),'Retirement Pension')]")
	public WebElement Retirement_PensionLink;

	@FindBy(xpath = "//h6[contains(text(),'Knowledge Center')]")
	public WebElement KnowledgeCenterLabel;

	@FindBy(xpath = "//span[contains(text(),'Retirement')]")
	public WebElement RetirementLink;

	@FindBy(xpath = "//span[contains(text(),'Money Management')]")
	public WebElement MoneyManagementLink;

	@FindBy(xpath = "//h6[contains(text(),'UPLOAD DOCUMENTS')]")
	public WebElement UploadDocsLabel;

	@FindBy(xpath = "//span[contains(text(),'General Documents')]")
	public WebElement GeneralDocumentsLink;

	@FindBy(xpath = "//span[contains(text(),'Overview')]")
	public WebElement OverviewLink_hm;
	
	@FindBy(xpath = "//span[contains(text(),'Tax Withholding')]")
	public WebElement TaxWithHoldinglink_hm;
	
	@FindBy(xpath = "//span[contains(text(),'Payment Method')]")
	public WebElement PaymentMethodlink_hm;
	
	@FindBy(xpath = "//h6[contains(text(),'Covestro Pension Plan')]")
	public WebElement CovestroPensionPlanHeader;

	
	@FindBy(xpath = "//a[contains(text(),'Logout')]")
	public WebElement logoutBtn;
	
	
	//-----------------------------------------Multi-Plan Elements---------------------------------------------//
	
	@FindBy(xpath = "(//div[@class='prepared']/a/span[contains(text(),'E')])[2]")
	public WebElement MP_EstNowBtn;
	
	@FindBy(css = "div[class*='btns-group'] > button[ng-click='closePopup(false)']")
	public WebElement PopOKBtn;
	
	@FindBy(xpath = "//h2[contains(text(),'Multi-Plan Estimator Tool')]")
	public WebElement MPEstimatorLabel;
	
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(1) > a > span.ng-binding")
	public WebElement EstYourMPPensionBenTab;
	
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(2) > a > span.ng-binding")
	public WebElement MstRcntMPEstTab;
	
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(3) > a > span.ng-binding")
	public WebElement SavedMPEstTab;
	
	@FindBy(xpath = "//span[contains(text(),'Estimate Your Multi-Plan Pension Benefits')]")
	public WebElement EstYourMPPensionBenLabel;
	
	@FindBy(xpath = "//div[@class='estimate-steps']")
	public WebElement Est_Steps;
	
	@FindBy(xpath = "//span[contains(text(),'Multi-Plan Estimate Results')]")
	public WebElement MPEstResultsLabel;
	
	@FindBy(xpath = "//div[@class='multi-plan-calc-comparison-details']")
	public WebElement MP_CCDetails;
	
	@FindBy(xpath = "//button[contains(text(),'View Assumptions')]")
	public WebElement ViewAsmptnsBtn;
	
	@FindBy(xpath = "//div[@class='black-popup animate-collapsible-block assumptions opened']")
	public WebElement AsmptnsDetails;
	
	@FindBy(xpath = "//button[@class='close']")
	public WebElement AsmptnsClose;
	
	@FindBy(xpath = "//span[contains(text(),'Saved Multi-Plan Estimates')]")
	public WebElement SavedMPEstLabel;
	
	@FindBy(xpath = "//div[@class='saved-estimate-wrap']")
	public WebElement SavedMPDetails;
	
	@FindBy(xpath = "//span[contains(text(),'Multi-Plan Estimator Tool')]")
	public WebElement MPEstimatorTile;
	
	
	//---------------------------------------------ROL Elements---------------------------------------------//
	
	@FindBy(xpath = "//span[contains(text(),'Not Started')]")
	public WebElement NotStartedLabel;
	
	//@FindBy(xpath = "span[contains(text(),'Get Started')]")
	@FindBy(css = "button[ng-click*='getStarted()']")
	public WebElement GetStartedBtn;
	
	@FindBy(css = "button[ng-click*='resume']")
	public WebElement ResumeBtn;
	
	@FindBy(css = "a[ng-click*='Delete']")
	public WebElement CancelCommLink;
	
	@FindBy(css = "div.status-info > span.status-progress.ng-scope >span")
	public WebElement StatusLabel;
	
	
	//-------------------------------------------BW Avon Elements------------------------------------------//
	
	@FindBy(xpath = "//span[contains(text(),'ADP Voluntary Early Retirement Program')]")
	public WebElement ADPTile;
		
	@FindBy(xpath = "//span[contains(text(),'My Elections')]")
	public WebElement MyElectionsLink;
	
	@FindBy(css = "a[data-ng-href='/db-plan/summary/AVON']")
	public WebElement PlanOneLinkHm;
	
	@FindBy(css = "a[data-ng-href='/db-plan/summary/AVONNQ']")
	public WebElement PlanTwoLinkHm;
	
	
	//----------------------------------------Cash Balance Elements----------------------------------------//
	
	@FindBy(xpath = "//h3[contains(text(),'MBC Test Desc1 - Personal Retirement Account Plan')]")
	public WebElement AVONPlantitle;
	
	@FindBy(xpath = "//h3[contains(text(),'MBC Test1 Desr2 - Personal Retirement Account Plan')]")
	public WebElement AVONPlantitle2;
	
	@FindBy(xpath = "//span[@class='ref-date ng-binding']")
	public WebElement Asoftext;
	
	@FindBy(xpath = "//span[contains(text(),'Balances')]")
	public WebElement BalanceLabel;
	
	@FindBy(xpath = "//h1[@class='ng-binding']")
	public WebElement currentbalanceamount;
	
	@FindBy(xpath = "//*[@class='highcharts-background']")
	public WebElement CBGraph;
	
	@FindBy(xpath = "//div[@class='highcharts-axis-labels highcharts-xaxis-labels'] /span[3]")
	public WebElement xaxisgraph;
	
	@FindBy(xpath = "//div[@class='highcharts-axis-labels highcharts-yaxis-labels'] /span[3]")
	public WebElement yaxisgraph;
	
	@FindBy(xpath = "//span[contains(text(),'Beginning Balance')]")	
	public WebElement CBbeginningbalance;
	
	@FindBy(xpath = "//span[contains(text(),'Contribution')]")
	public WebElement CBcontribution;
	
	@FindBy(xpath = "//span[contains(text(),'Interest')]")
	public WebElement CBInterest;
	
	@FindBy(xpath = "//span[contains(text(),'Adjustment')]")
	public WebElement CBadjustment;
	
	@FindBy(xpath = "(//*[@fill='#00A8C8'])[7]")
	public WebElement CBbeginningbalancecolor;
	
	@FindBy(xpath = "(//*[@fill='#203976'])[7]")
	public WebElement CBcontributioncolor;
	
	@FindBy(xpath = "(//*[@fill='#8CBD4F'])[7]")
	public WebElement CBInterestcolor;
	
	@FindBy(xpath = "(//*[@fill='#70973F'])[7]")
	public WebElement CBadjustmentcolor;
	
	
	@FindBy(css = "g[class='highcharts-markers highcharts-tracker']")
	public WebElement cbgraphbarparent;
	
	//g[class='highcharts-series highcharts-tracker']:nth-of-type(7)
	@FindBy(xpath = "(//*[@class='highcharts-series highcharts-tracker'])[8]") 
	public WebElement psgraphbarparent;
	
	@FindBy(xpath = "(//*[@fill='#ffffff'])[1]")
	public WebElement cbgraphbar1;
	
	@FindBy(xpath = "(//*[@fill='#00A8C8'])[6]")
	public WebElement psgraphbar1;
	
	@FindBy(xpath = "(//span[@class='title ng-binding'])[1]")
	public WebElement beginningbalancepopup;
	
	@FindBy(xpath = "(//span[@class='title ng-binding'])[2]")
	public WebElement contributionepopup;
	
	@FindBy(xpath = "(//span[@class='title ng-binding'])[3]")
	public WebElement Interestpopup;
	
	@FindBy(xpath = "(//span[@class='title ng-binding'])[4]")
	public WebElement adjustmentpopup;
	
	@FindBy(xpath = "(//span[@class='title ng-binding'])[5]")
	public WebElement endingbalancepopup;
	
	@FindBy(xpath = "(//span[@class='value ng-binding'])[1]")
	public WebElement beginningbalancevaluepopup;
	
	@FindBy(xpath = "(//span[@class='value ng-binding'])[2]")
	public WebElement contributionevaluepopup;
	
	@FindBy(xpath = "(//span[@class='value ng-binding'])[3]")
	public WebElement Interestvaluepopup;
	
	@FindBy(xpath = "(//span[@class='value ng-binding'])[4]")
	public WebElement adjustmentvaluepopup;
	
	@FindBy(xpath = "(//span[@class='value ng-binding'])[5]")
	public WebElement endingbalancevaluepopup;
	
	@FindBy(xpath = "//span[@id='closeChartPopup']/span[@class='icomn-close']")
	public WebElement closepopup;
	
	@FindBy(xpath = "//span[contains(text(),'Important Dates')]")
	public WebElement ImportantDatesButton;
	
	@FindBy(xpath = "//h5[contains(text(),'Important Dates')]")
	public WebElement ImportantDatesLabel;
	
	@FindBy(xpath = "//button/span[contains(text(),'Back')]")
	public WebElement IDBackButton;
	
	@FindBy(xpath = "//span[contains(text(),'View Details and Run Estimates')]")
	public WebElement ViewEstimateLinkOnTile;
	
	
	//--------------------------------------------DS Hybrid Elements-------------------------------------------//
	
	@FindBy(xpath = "//h3[contains(text(),'ADP Pension Retirement Plan')]")
	public WebElement HybPlantitle;
	
	@FindBy(xpath = "//span[contains(text(),'Active Portion')]")
	public WebElement APLabel;
	
	@FindBy(xpath = "//span[contains(text(),'Frozen Portion')]")
	public WebElement FPLabel;
	
	@FindBy(xpath = "(//span[@class='ref-date ng-binding'])[1]")
	public WebElement APAsoftext;
	
	@FindBy(xpath = "(//span[@class='ref-date ng-binding'])[2]")
	public WebElement FPAsoftext;
	
	@FindBy(xpath = "(//span[contains(text(),'Balances')])[1]")
	public WebElement APBalanceLabel;
	
	@FindBy(xpath = "(//h1[@class='ng-binding'])[1]")
	public WebElement APcurrentbalanceamount;
	
	@FindBy(xpath = "(//h1[@class='ng-binding'])[2]")
	public WebElement FPcurrentbalanceamount;
	
	
	//------------------------------------------------BS  Elements----------------------------------------------//
	
	@FindBy(xpath = "//div[@class='panel-body']")
	public WebElement BSummaryTable;
	
	@FindBy(xpath = "(//div[@class='panel-body']/div/div/div[5])[1]")
	public WebElement BSummaryTableBalCol;
	
	@FindBy(xpath = "(//div[@class='panel-body']/div/div/div[5])[2]")
	public WebElement BSummaryTableBalance;
	
	
	//-----------------------------------------------BS  Methods-----------------------------------------------//
	
	public void VerifyDB_BSummary(ExtentTest test) throws InterruptedException {
		verifyElementIsPresent(BSummaryTable, test, "Benefit Summary Table");
		verifyElementIsPresent(BSummaryTableBalCol, test, "Benefit Summary Table Balance Column");
		verifyElementIsPresent(BSummaryTableBalance, test, "Benefit Summary Table Balance");
	}
	
	
	//-----------------------------------------Future Payment Elements-----------------------------------------//
	
	@FindBy(xpath = "//div/div/div/div/h2/picture[contains(text(),'*')]")
	public WebElement FP_symbol;
	
	@FindBy(xpath = "//div/div/div/div/h2/picture[contains(text(),'*')]//following::div/span/span/a[contains(text(),'Click here')]")
	public WebElement FP_DB_ClickHere;
	
	//-----------------------------------------Future Payment  Methods-----------------------------------------//
	
	public void VerifyDB_FP_Flow(WebDriver driver, ExtentTest test) throws InterruptedException {
		verifyElementTextContains(FP_symbol, "*", test);
		verifyElementTextContains(FP_DB_ClickHere, "Click here", test);
		clickElement(FP_DB_ClickHere);
		InPayPage Inpayop = new InPayPage(driver);
		waitForElementToDisplay(Inpayop.FuturePaymentChangesLabel);
		verifyElementTextContains(Inpayop.FuturePaymentChangesLabel, "Future Payment Changes", test);
		verifyElementIsPresent(Inpayop.futurePaymentForm, test, "Future Payment Details");
		driver.navigate().back();
	}
	
	//------------------------------------------Compare Calc Elements------------------------------------------//
	
	@FindBy(xpath = "//span[contains(text(),'Kellogg Heritage')]")
	public WebElement KelloggHeritageLink;
	
	
	//-------------------------------------------Doc Upload Elements-------------------------------------------//
	
	@FindBy(xpath = "//h2[contains(text(), 'UPLOAD DOCUMENTS')]")
	public WebElement UploadDocLabel;
	
	@FindBy(xpath = "//h2[contains(text(), 'General Documents')]")
	public WebElement GenDocsLabel;
	
	@FindBy(xpath ="//dl[@class='upload-conditions']")
	public WebElement UpldConditions;
	
	@FindBy(xpath = "//a[contains(text(), 'Select File')]")
	public WebElement SelectFileBtn;
	
	@FindBy(xpath = "//div[@class='col col-select']/input")
	public WebElement UpLdDocInputBtn;
	
	@FindBy(xpath = "//li/span[contains(text(), 'Other Document')]")
	public WebElement OptionODocs;
	
	@FindBy(xpath = "//button[contains(text(), 'Upload')]")
	public WebElement UploadDocBtn;
	
	@FindBy(xpath = "//div[@class='pop-up']/div/h3/span[contains(text(), 'Success')]")
	public WebElement SuccessHeader;
	
	@FindBy(xpath = "//div[@class='pop-up']/div/p/span[contains(text(), 'Your document was uploaded.')]")
	public WebElement SuccessMsgConent;
	
	@FindBy(xpath = "//div[@class='pop-up']/div/div/button")
	public WebElement Close_SMsg_PopUp;
	
	@FindBy(xpath = "//div[contains(text(), 'This file type cannot be processed. Allowed file types are PNG, PDF, GIF, TIF or TIFF, BMP, JPG, or JPEG.')]")
	public WebElement AlertMessage_InvalidFile;
	
	
	//------------------------------------------Dashboard Methods----------------------------------------------//	
	
	public void logoutDB() throws InterruptedException {
		clickElement(logoutBtn);
	}
	
	public void verifycurrentbalance(ExtentTest test, WebElement webEle, String element) {
		String curbal = webEle.getText();
		String curbalvalue = curbal.substring(1);
		String curbalformat = "$"+curbalvalue;
		System.out.println(curbalformat+" "+curbal);
		if(curbalformat.equals(curbal)) {
			test.log(Status.PASS, "verifyElementIsPresent()" + " Webelement " + webEle.getAttribute("outerHTML") + element
					+ " is Displayed and Amount Format is verified.");
		} else {
			test.log(Status.FAIL,
					"verifyElementIsPresent() " + " Webelement " + webEle.getAttribute("outerHTML") + " have actual ");
		}
	}
	
	//----------------------------------------Cash Balance Methods---------------------------------------------//
	
	public void verifyGraphPopup(ExtentTest test, String flow)throws InterruptedException{
		if(flow=="Dashboard") {
			hoverAndClickOnElement(driver, cbgraphbarparent, cbgraphbar1);
		}else if(flow=="PlanSummary") {
			//scrollToElement(driver, psgraphbarparent);
			hoverAndClickOnElement(driver, psgraphbarparent, psgraphbar1);
			//clickElement(psgraphbar1);
		}
		waitForElementToDisplay(beginningbalancepopup);
		verifyElementIsPresent(beginningbalancepopup, test, "Beginning Balance Popup Tab");
		verifyElementIsPresent(contributionepopup, test, "Contribution Popup Tab");
		verifyElementIsPresent(Interestpopup, test, "Interest Popup Tab");
		verifyElementIsPresent(adjustmentpopup, test, "Adjustment Popup Tab");
		verifyElementIsPresent(endingbalancepopup, test, "Endingbalance Popup Tab");
		verifyElementIsPresent(beginningbalancevaluepopup, test, "Beginning Balance Popup Value");
		verifyElementIsPresent(contributionevaluepopup, test, "Contribution Popup Value");
		verifyElementIsPresent(Interestvaluepopup, test, "Interest Popup Value");
		verifyElementIsPresent(adjustmentvaluepopup, test, "Adjustment Popup Value");
		verifyElementIsPresent(endingbalancevaluepopup, test, "Endingbalance Popup Value");
		clickElement(closepopup);
	}
	
	//-------------------------------------------Doc Upload Methods-------------------------------------------//
	
	public void DocUpload(ExtentTest test, String type)throws InterruptedException{
		waitForElementToDisplay(SelectFileBtn);
		clickElement(SelectFileBtn);
		File file = null;
		if(type=="Valid") {
			file = new File("./src/main/resources/testdata/Test_Document.pdf");
		}else if(type=="Invalid"){
			file = new File("./src/main/resources/testdata/Test_Doc.docx");
		}
		UpLdDocInputBtn.sendKeys(file.getAbsolutePath());
		clickElement(OptionODocs);
		waitForElementToDisplay(UploadDocBtn);
		clickElement(UploadDocBtn);
		if(type=="Valid") {
			waitForElementToDisplay(SuccessHeader);
			verifyElementTextContains(SuccessHeader, "Success", test);
			verifyElementTextContains(SuccessMsgConent, "Your document was uploaded.", test);
			clickElement(Close_SMsg_PopUp);
			verifyElementTextContains(UploadDocLabel, "UPLOAD DOCUMENTS", test);
		}else if(type=="Invalid"){
			waitForElementToDisplay(AlertMessage_InvalidFile);
			verifyElementIsPresent(AlertMessage_InvalidFile, test, "Invalid File - Alert Msg");
		}
	}
	
}

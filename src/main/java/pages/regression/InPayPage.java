package pages.regression;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementIsNotPresent;
import static verify.SoftAssertions.verifyElementTextContains;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

public class InPayPage {
	WebDriver driver;

	public InPayPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	// ---------------------------------------Inpay Elements------------------------------------------//

	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(2) > a")
	public WebElement TaxWithHoldingTab;

	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(3) > a")
	public WebElement PaymentMethodTab;

	@FindBy(xpath = "//a[text()='Summary']")
	public WebElement SummaryLink;

	@FindBy(xpath = "//a[text()='Payment History']")
	public WebElement PaymentHistoryLink;

	@FindBy(xpath = "//a[text()='Scheduled Deductions']")
	public WebElement ScheduledDeductionsLink;
	
	@FindBy(xpath = "//a[text()='Future Payment Changes']")
	public WebElement FuturePaymentChangesLink;

	@FindBy(xpath = "//span[contains(text(),'Payment Summary')]")
	public WebElement PaymentSummaryLabel;

	@FindBy(xpath = "//span[contains(text(),'Payment History')]")
	public WebElement PaymentHistoryLabel;

	@FindBy(xpath = "//h2[contains(text(),'Scheduled Deductions')]")
	public WebElement ScheduledDeductionsLabel;
	
	@FindBy(xpath = "//h2[contains(text(),'Future Payment Changes')]")
	public WebElement FuturePaymentChangesLabel;
	
	@FindBy(xpath = "//form[@name='futurePaymentForm']")
	public WebElement futurePaymentForm;

	@FindBy(xpath = "//a[text()='Federal']")
	public WebElement FederalLink;

	@FindBy(xpath = "//a[text()='State']")
	public WebElement StateLink;

	@FindBy(xpath = "//span[contains(text(),'Federal Tax Withholding')]")
	public WebElement FederalTaxWithHoldingLabel;

	@FindBy(xpath = "//span[contains(text(),'State Tax Withholding')]")
	public WebElement StateTaxWithHoldingLabel;

	@FindBy(xpath = "//h2[contains(text(),'Payment Method')]")
	public WebElement PaymentMethodLabel;

	// ---------------------------------------Federal Elements------------------------------------------//

	@FindBy(xpath = "//span[contains(text(),'Edit')]")
	public WebElement FEditbutton;

	@FindBy(xpath = "//h3[contains(text(),'Edit Federal Tax Withholding')]")
	public WebElement FEdittitle;

	@FindBy(xpath = "(//span[contains(text(),'Download Worksheet')])[1]")
	public WebElement DownloadWSlink;

	@FindBy(xpath = "(//div[@class='table-wrp column6'])[1]")
	public WebElement Fedtable;

	@FindBy(xpath = "(//div[contains(text(),'Payment')])[1]")
	public WebElement Federalpaymenttitle;

	@FindBy(xpath = "(//div[contains(text(),'Gross Amount')])[1]")
	public WebElement FGrossamounttitle;

	@FindBy(xpath = "(//div[contains(text(),'Withholding')])[1]")
	public WebElement FWithholdingtitle;

	@FindBy(xpath = "(//div[contains(text(),'Allowances')])[1]")
	public WebElement FAllowancestitle;

	@FindBy(xpath = "(//div[contains(text(),'Marital Status For Tax Withholding')])[1]")
	public WebElement FMaritalStatustitle;

	@FindBy(xpath = "(//div[contains(text(),'Additional Amount (Optional)')])[1]")
	public WebElement FAdditionalamounttitle;

	@FindBy(xpath = "(//span[contains(text(),'Yes')])[1]")
	public WebElement Fwithholdingdropdown;

	@FindBy(xpath = "(//span[contains(text(),'No')])[1]")
	public WebElement Fwithholdingdropdown1;

	@FindBy(xpath = "(//a[contains(text(),'Yes')])[1]")
	public WebElement FchooseYes;

	@FindBy(xpath = "(//a[contains(text(),'No')])[1]")
	public WebElement FchooseNo;

	@FindBy(xpath = "(//span[@class='fa fa-angle-down fa-lg'])[1]")
	public WebElement Fchoose;

	@FindBy(xpath = "(//input[@name='allowances'])[1]")
	public WebElement Fallowancetextbox;

	@FindBy(xpath = "(//span[contains(text(),'None')])[1]")
	public WebElement Fmaritaldropdownbox;

	@FindBy(xpath = "(//span[contains(text(),'Single')])[1]")
	public WebElement Fmarital_single_dropdownbox;

	@FindBy(xpath = "(//span[contains(text(),'Married')])[1]")
	public WebElement Fmarital_married_dropdownbox;

	@FindBy(xpath = "(//a[contains(text(),'None')])[1]")
	public WebElement Fmaritalnonechose;

	@FindBy(xpath = "(//a[contains(text(),'Single')])[1]")
	public WebElement Fmaritalsinglechose;

	@FindBy(xpath = "(//a[contains(text(),'Married')])[1]")
	public WebElement Fmaritalmarriedchose;

	@FindBy(xpath = "//input[@name='additionalAmount']")
	public WebElement Famounttextbox;

	@FindBy(xpath = "//div[@class='checkbox checkbox-success']/label/span")
	public WebElement checkbox;

	@FindBy(xpath = "(//button[contains(text(),'Cancel')])[2]")
	public WebElement cancelbutton;

	@FindBy(xpath = "//span[contains(text(),'Your changes have not been saved.')]")
	public WebElement EditCancelMsg;

	@FindBy(xpath = "//button[contains(text(),'Save')]")
	public WebElement savebutton;

	@FindBy(xpath = "//span[contains(text(),'Your changes have been saved.')]")
	public WebElement EditSaveMsg;
	
	
	// ----------------------------------------State Elements-------------------------------------------//

	@FindBy(xpath = "//span[contains(text(),'Change State')]")
	public WebElement changestateLink;
	
	@FindBy(xpath = "//span[contains(text(),'Select a State')]")
	public WebElement selectStateLabel;
	
	@FindBy(xpath = "//button[@class='btn dropdown-toggle selectpicker btn-default']")
	public WebElement changestateDropdown;
	
	@FindBy(xpath = "(//div[@class='checkbox checkbox-success']/label/span)[1]")
	public WebElement InScheckbox1;
	
	@FindBy(xpath = "(//div[@class='checkbox checkbox-success']/label/span)[2]")
	public WebElement InScheckbox2;
	
	@FindBy(xpath = "(//div[@class='checkbox checkbox-success']/label/span)[3]")
	public WebElement InScheckbox3;
	
	@FindBy(xpath = "(//button[contains(text(),'Cancel')])[3]")
	public WebElement SChngCancel;
	
	@FindBy(xpath = "(//button[contains(text(),'Save')])[2]")
	public WebElement SChngSave;
	
	@FindBy(xpath = "//p[contains(text(),'There is no income tax assessed in')]")
	public WebElement NoTaxDesc;
	
	@FindBy(xpath = "//span[contains(text(),'Edit')]")
	public WebElement SEditbutton;

	@FindBy(xpath = "//h3[contains(text(),'Edit Alaska Tax Withholding')]")
	public WebElement SEdittitle;
	
	@FindBy(xpath = "(//input[@name='amount'])[2]")
	public WebElement Samountbox;
	
	
	// ---------------------------------------Federal Methods------------------------------------------//

	public void federaledit(String action, ExtentTest test) throws InterruptedException {
		clickElement(FEditbutton);
		waitForElementToDisplay(FEdittitle);
		verifyElementTextContains(FEdittitle, "Edit Federal Tax Withholding", test);
		if (Fwithholdingdropdown.isDisplayed()) {
			waitForElementToDisplay(Fallowancetextbox);
			verifyElementIsPresent(Fallowancetextbox, test, "Allowance Input Element");
			verifyElementIsPresent(Famounttextbox, test, "Amount Input Element");
			Fallowancetextbox.clear();
			Fallowancetextbox.sendKeys(randnum(80, 30));
			maritalselection("Single");
			Famounttextbox.clear();
			Famounttextbox.sendKeys(randnum(1000, 0));
			verifyElementIsPresent(checkbox, test, "Checkbox");
			actionflow(action, test);
		} else if (Fwithholdingdropdown1.isDisplayed()) {
			clickElement(Fchoose);
			clickElement(FchooseYes);
			waitForElementToDisplay(Fallowancetextbox);
			verifyElementIsPresent(Fallowancetextbox, test, "Allowance Input Element");
			verifyElementIsPresent(Famounttextbox, test, "Amount Input Element");
			Fallowancetextbox.clear();
			Fallowancetextbox.sendKeys(randnum(80, 30));
			maritalselection("Single");
			Famounttextbox.clear();
			Famounttextbox.sendKeys(randnum(1000, 0));
			verifyElementIsPresent(checkbox, test, "Checkbox");
			actionflow(action, test);
		}
	}
	
	public void federaleditNoTW(String action, ExtentTest test) throws InterruptedException {
		clickElement(FEditbutton);
		waitForElementToDisplay(FEdittitle);
		verifyElementTextContains(FEdittitle, "Edit Federal Tax Withholding", test);
		if (isElementExisting(driver, Fwithholdingdropdown)) {
			clickElement(Fchoose);
			clickElement(FchooseNo);
			//verifyElementIsNotPresent(Fallowancetextbox, test, "Allowance Input Element");
			verifyElementIsNotPresent(Famounttextbox, test, "Amount Input Element");
			verifyElementIsPresent(checkbox, test, "Checkbox");
			actionflow(action, test);
		}else if(isElementExisting(driver, Fwithholdingdropdown1)) {
			//verifyElementIsNotPresent(Fallowancetextbox, test, "Allowance Input Element");
			verifyElementIsNotPresent(Famounttextbox, test, "Amount Input Element");
			verifyElementIsPresent(checkbox, test, "Checkbox");
			actionflow(action, test);
		}
	}

	public void maritalselection(String choice) throws InterruptedException {
		if (choice == "Single") {
			if (Fmarital_single_dropdownbox.isDisplayed()) {
				clickElement(Fmarital_single_dropdownbox);
				clickElement(Fmaritalsinglechose);
			} else if (Fmarital_married_dropdownbox.isDisplayed()) {
				clickElement(Fmarital_married_dropdownbox);
				clickElement(Fmaritalsinglechose);
			} else if (Fmaritaldropdownbox.isDisplayed()) {
				clickElement(Fmaritaldropdownbox);
				clickElement(Fmaritalsinglechose);
			} 
		} else if (choice == "Married") {
			 if (Fmarital_single_dropdownbox.isDisplayed()) {
				clickElement(Fmarital_single_dropdownbox);
				clickElement(Fmaritalmarriedchose);
			} else if (Fmarital_married_dropdownbox.isDisplayed()) {
				clickElement(Fmarital_married_dropdownbox);
				clickElement(Fmaritalmarriedchose);
			} else if (Fmaritaldropdownbox.isDisplayed()) {
				clickElement(Fmaritaldropdownbox);
				clickElement(Fmaritalmarriedchose);
			}
		}
	}

	public void actionflow(String action, ExtentTest test) throws InterruptedException {
		if (action == "Cancel") {
			verifyElementIsPresent(cancelbutton, test, "Cancel Button");
			clickElement(cancelbutton);
			waitForElementToDisplay(EditCancelMsg);
			verifyElementTextContains(EditCancelMsg, "Your changes have not been saved.", test);
		} else if (action == "Save") {
			clickElement(checkbox);
			clickElement(InScheckbox2);
			verifyElementIsPresent(savebutton, test, "Save Button");
			clickElement(savebutton);
			waitForElementToDisplay(EditSaveMsg);
			verifyElementTextContains(EditSaveMsg, "Your changes have been saved.", test);
		}
	}

	public static String randnum(int Max, int Min) {
		Random rand = new Random();
		int rand_int = rand.nextInt((Max - Min) + 1) + Min;
		return Integer.toString(rand_int);
	}
	
	
	// ---------------------------------------State Methods------------------------------------------//
	
	public void stateChange(String state, String action, ExtentTest test) throws InterruptedException {
		WebElement Lb;
		clickElement(changestateLink);
		waitForElementToDisplay(selectStateLabel);
		verifyElementTextContains(selectStateLabel, "Select a State", test);
		waitForElementToDisplay(changestateDropdown);
		clickElement(changestateDropdown);
		driver.findElement(By.xpath("//span[contains(text(),'"+state+"')]")).click();
		Lb = driver.findElement(By.xpath("//span[contains(text(),'Edit "+state+" Tax Withholding')]"));
		waitForElementToDisplay(Lb);
		verifyElementTextContains(Lb, "Edit "+state+" Tax Withholding", test);
		actionSflow(action, test);
	}
	
	public void actionSflow(String action, ExtentTest test) throws InterruptedException {
		if (action == "Cancel") {
			verifyElementIsPresent(SChngCancel, test, "Cancel Button");
			clickElement(SChngCancel);
			waitForElementToDisplay(EditCancelMsg);
			verifyElementTextContains(EditCancelMsg, "Your changes have not been saved.", test);
		} else if (action == "Save") {
			Boolean val = isElementExisting(driver, NoTaxDesc);
			if (val == true) {
				clickElement(InScheckbox1);
				//clickElement(InScheckbox2);
			} else if (val == false) {
				clickElement(InScheckbox1);
				clickElement(InScheckbox2);
				clickElement(InScheckbox3);
			}			 
			verifyElementIsPresent(SChngSave, test, "Save Button");
			clickElement(SChngSave);
			waitForElementToDisplay(EditSaveMsg);
			verifyElementTextContains(EditSaveMsg, "Your changes have been saved.", test);
		}
	}
	
	
	//----------------------------------------Payment Elements------------------------------------------//
	
	@FindBy(xpath = "//li[@class='alert-header-ctn']")
	public WebElement WTDH_Sticker_Label;
	
	@FindBy(xpath = "(//span[contains(text(),'What To Do Here')])[2]")
	public WebElement WTDH_Text_Label;
	
	@FindBy(xpath = "//li[@id='WhatToDoHereContent']")
	public WebElement WTDH_Label_Cnt;
	
	@FindBy(xpath = "//button/span/span[@class='icomn-close']")
	public WebElement Close_WTDH;
	
	@FindBy(xpath = "//div[@class='notification-tack']")
	public WebElement WTDH_Ntfctn_Tack;
	
	@FindBy(xpath = "(//span[contains(text(),'What To Do Here')])[1]")
	public WebElement WTDH_Ntfctn_Text_Label;
	
	@FindBy(xpath = "//div[@class='page-pension-payment-summary ng-scope']/div[2]")
	public WebElement PaymentSummaryDetails;
	
	@FindBy(xpath = "//div[@class='dropdown dropdown-inline dropdown-xl']")
	public WebElement PaymentPlanDropDown;
	
	@FindBy(xpath = "//span[contains(text(),'Payment Start:')]")
	public WebElement PaymentStartDateLabel;
	
	@FindBy(xpath = "//div[@class='page-pension-payment-history ng-scope']/div/span[2]")
	public WebElement PaymentStartDate;
	
	@FindBy(xpath = "//div[@data='currentHistory']")
	public WebElement CurrentHistoryDetails;
	
	@FindBy(xpath = "//div[contains(text(),'Year')]")
	public WebElement CurHis_YearLabel;
	
	@FindBy(xpath = "//div[contains(text(),'Gross Payment Amount')]")
	public WebElement CurHis_GPALabel;

	@FindBy(xpath = "//div[contains(text(),'Payment Method')]")
	public WebElement CurHis_PMLabel;
	
	@FindBy(xpath = "//div[contains(text(),'Detailed Report')]")
	public WebElement CurHis_DPLabel;
	
	@FindBy(xpath = "//a/span[contains(text(),'Payment Details')]")
	public WebElement PaymentDetailsLinks;
	
	@FindBy(xpath = "//h2[contains(text(),'Payment Details')]")
	public WebElement PaymentDetailsLabel;
	
	@FindBy(xpath = "//div[@class='table-wrp payment-details-wrp']")
	public WebElement PaymentDetails;
	
	
	//----------------------------------------Payment Methods------------------------------------------//
	
	public void check_WTDH(ExtentTest test) throws InterruptedException {
		waitForElementToDisplay(WTDH_Sticker_Label);
		verifyElementIsPresent(WTDH_Sticker_Label, test, "WTDH Label");
		verifyElementTextContains(WTDH_Text_Label, "What To Do Here", test);
		verifyElementIsPresent(WTDH_Label_Cnt, test, "WTDH Label Content");
		clickElement(Close_WTDH);
		waitForElementToDisplay(WTDH_Ntfctn_Tack);
		verifyElementIsPresent(WTDH_Ntfctn_Tack, test, "WTDH Notification Tack");
		verifyElementTextContains(WTDH_Ntfctn_Text_Label, "What To Do Here", test);
		clickElement(WTDH_Ntfctn_Text_Label);
		waitForElementToDisplay(WTDH_Sticker_Label);
		verifyElementIsPresent(WTDH_Sticker_Label, test, "WTDH Label");
	}
	
}

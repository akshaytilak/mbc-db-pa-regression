package pages.regression;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.doubleClickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.hoverAndClickOnElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyEquals;

import java.text.DecimalFormat;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import driverfactory.Driver;


public class EstimatePage {
	WebDriver driver;
	public EstimatePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	
	//------------------------------------------Estimate Page Elements------------------------------------------//
	
	@FindBy(xpath="//span[contains(text(),'Overview')]")
	public WebElement OverviewHRtab;
	
	/*@FindBy(xpath="//span[contains(text(),'Estimates')]")
	public WebElement EstimatesTab;
	
	@FindBy(xpath="//span[contains(text(),'My Data')]")
	public WebElement MyDataTab;
	
	@FindBy(xpath = "//span[contains(text(),'Your Plan Summary')]")
	//span[contains(text(),'Action Needed')]
	public WebElement PlanSummaryTab;*/
	
	
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(1) > a]")
	public WebElement OverviewTab;

	// @FindBy(xpath = "//span[text()='Estimates']")
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(2) > a")
	public WebElement EstimatesTab;

	// @FindBy(xpath = "//span[text()='My Data']")
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(3) > a")
	public WebElement MyDataTab;
	
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(4) > a")
	public WebElement MyElectionsTab;

	@FindBy(xpath = "//span[contains(text(),'Your Plan Summary')]")
	public WebElement YourPlanSummaryLabel;

	@FindBy(xpath = "//a[text()='Estimate Your Pension']")
	public WebElement EstimateYourPensionLink;

	@FindBy(xpath = "//span[contains(text(),'Estimate Your Pension')]")
	public WebElement EstimateYourPensionLabel;

	@FindBy(xpath = "//a[text()='Most Recent Estimate']")
	public WebElement MostRecentEstimateLink;
	
	@FindBy(xpath = "//h2[contains(text(),'Estimate Details')]")
	public WebElement EstimateDetailsLabel;

	@FindBy(xpath = "//a[text()='Saved Estimates']")
	public WebElement SavedEstimatesLink;

	@FindBy(xpath = "//span[contains(text(),'Saved Estimates')]")
	public WebElement SavedEstimatesLabel;

	@FindBy(xpath = "//h2[contains(text(),'My Data')]")
	public WebElement MyDataLabel;
	
	@FindBy(xpath = "//h2[contains(text(),'My Elections')]")
	public WebElement MyElectionsLabel;
		
	@FindBy(xpath = "//span[contains(text(),'Normal Retirement')]")
	public WebElement NormalRetirementLabel;

	@FindBy(xpath = "(//i[@class='fa fa-stack-1x fa-lg fa-circle-o'])[2]")
	public WebElement Normalretirementradiobutton;
	
	@FindBy(xpath = "//h4[contains(text(),'Beneficiary for Joint & Survivor Payments')])")
	public WebElement BeneficiariesLabel;
		
	@FindBy(xpath = "(//i[@class='fa fa-stack-1x fa-circle-o'])[1]") //--if Single not selected already
	public WebElement Singlemaritalstatusselect;
	
	@FindBy(xpath = "(//i[@class='fa fa-stack-1x fa-circle'])")
	public WebElement Singlemaritalstatus;
	
	@FindBy(xpath = "//a/span/i[@class='fa fa-stack-1x fa-circle']//following::label")
	public WebElement DefaultStatus;
	
	public WebElement SelectStatus(WebDriver driver, String status) {
		WebElement selStat = driver.findElement(By.xpath("//label[text()='"+ status +"']//preceding-sibling::a/span/i[@class='fa fa-stack-1x fa-circle-o']"));
		return selStat;
	}
		
	@FindBy(xpath = "//label[contains(text(),'Single')]")
	public WebElement Singlelabel;
	
	@FindBy(xpath = "(//input[@name='dtPickerCurrentDate'])[2]")
	public WebElement BeneficiaryDOB;
	
	@FindBy(xpath = "//label[contains(text(),'Registered Domestic Partner Date of Birth')]")
	public WebElement RPD_Dob_Label;
	
	@FindBy(xpath = "(//input[@name='dtPickerCurrentDate'])[2]")
	public WebElement RDP_DOB;
	
	@FindBy(xpath = "//span[contains(text(),'Continue')]")
	public WebElement Continuebutton;
	
	@FindBy(xpath = "//h2[contains(text(),'Please Read This Important Information')]")
	public WebElement AcknowledgementHeader;
	
	@FindBy(xpath = "//span[contains(text(),'I understand that this is an estimate. Please calculate my Pension Estimate.')]")
	public WebElement Acknowledgementcheckbox;
	
	@FindBy(xpath = "//button[contains(text(),'Accept')]")
	public WebElement Acceptbutton;
	
	@FindBy(xpath = "//span[contains(text(),'Calculating Your Estimate')]")
	public WebElement calculateestimatelabel;
	
	@FindBy(xpath = "//span[contains(text(),'Your Estimate Has Been Calculated')]")
	public WebElement EstimateCalculatedLabel;
	
	@FindBy(xpath = "//h2[contains(text(),'Calculation Error')]")
	public WebElement EstimateCalculatedErrorLabel;
	
	@FindBy(xpath = "//button[contains(text(),'View Results')]")
	public WebElement Viewresultsbutton;
	
	@FindBy(xpath = "//h2[contains(text(),'Estimate Details')]")
	public WebElement Estimatedetailslabel;
	
	@FindBy(xpath = "//h5[contains(text(),'Single Life Annuity')]")
	public WebElement SLALabel;
	
	@FindBy(xpath = "//h3[contains(text(),'Assumptions Used for Displayed Pension Estimates')]")
	public WebElement AssumptionLabel;
	
	@FindBy(xpath = "//span[contains(text(),'Your Detailed Report')]")
	public WebElement Detailedreportlink;
	
	@FindBy(xpath = "//a[contains(text(),'View Assumptions')]")
	public WebElement Viewassumption;
	
	@FindBy(xpath = "//span[contains(text(),'Calculate Another Estimate')]")
	public WebElement calculateAnotherestimatebutton;
	
	
	//------------------------------------------Compare Calc Elements------------------------------------------//

	@FindBy(xpath = "(//div[@class='checkbox checkbox-success ng-scope'])[1]")
	public WebElement Calc1_ChckBx;
	
	@FindBy(xpath = "(//div[@class='checkbox checkbox-success ng-scope'])[2]")
	public WebElement Calc2_ChckBx;
	
	@FindBy(xpath = "(//div[@class='checkbox checkbox-success ng-scope'])[3]")
	public WebElement Calc3_ChckBx;
	
	@FindBy(xpath = "(//a[contains(text(),'Compare')])[1]")
	public WebElement CompareButton;
	
	@FindBy(xpath = "//h2[contains(text(),'Estimate Comparison')]")
	public WebElement EstimateComparisonLabel;
	
	@FindBy(xpath = "//h3[contains(text(),'Your Estimate Comparison')]")
	public WebElement YourEstimateComparisonLabel;
	
	@FindBy(xpath = "//button[contains(text(),'View Assumptions')]")
	public WebElement ViewAssumptionButton;
	
	@FindBy(xpath = "//h4[contains(text(),'Assumptions Used for Displayed Pension Estimates')]")
	public WebElement AssumptionHeaderLabel;
	
	@FindBy(xpath = "(//div[contains(text(),'LAST DAY WORKED')])[1]")
	public WebElement LDWAssumption;
	
	@FindBy(xpath = "//div[contains(text(),'AGE AT LAST DAY WORKED')]")
	public WebElement AgeAtLDWAssumption;		
	
	@FindBy(xpath = "//div[contains(text(),'BENEFIT COMMENCEMENT DATE')]")
	public WebElement BCDAssumption;
	
	@FindBy(xpath = "//div[contains(text(),'AGE AT BENEFIT COMMENCEMENT')]")
	public WebElement AgeAtBCDAssumption;
	
	@FindBy(xpath = "(//span[contains(text(),'Close')])[3]")
	public WebElement CloseAssumption;
		
	@FindBy(xpath = "//div[@class='calc-another-button text-right ng-scope']/button")
	public WebElement CalcAnotherEstimateButton;
	
	@FindBy(xpath = "//span[contains(text(),'Back')]")
	public WebElement CompareBackButton;
	
	//---------------------------------------------BS Hybrid Elements-------------------------------------------//
	
	@FindBy(xpath = "//a[text()='Plan Summary']")
	public WebElement HybPlanSummaryLink;
	
	@FindBy(xpath = "//span[contains(text(),'Plan Summary')]")
	public WebElement HybPlanSummaryLabel;
	
	@FindBy(xpath = "//span[contains(text(),'View History')]")
	public WebElement ViewHistoryLink;
	
	@FindBy(xpath = "//div[@class='graph-ctn ng-scope']")
	public WebElement BalanceHistoryGraph;
	
	@FindBy(xpath = "(//div[@class='gray-back balance'])[1]")
	public WebElement APDetailsCnt;
	
	@FindBy(xpath = "(//div[@class='gray-back balance'])[2]")
	public WebElement FPDetailsCnt;
	
	@FindBy(xpath = "//div[@class='gray-back imp-dates ng-scope']")
	public WebElement ImpDatesDetailsCnt;
	
	@FindBy(xpath = "//a[text()='History']")
	public WebElement HybHistoryLink;
	
	@FindBy(xpath = "//span[contains(text(),'History')]")
	public WebElement HybHistoryLabel;
	
	//-----------------------------------------Plan Summary CB Elements-----------------------------------------//
	
	@FindBy(xpath ="//span[contains(text(),'MBC Test1 Desr2 - Personal Retirement Account Plan')]")
	public WebElement planlink;
	
	@FindBy(xpath ="//h2[contains(text(),'Test DESCR1- Personal Retirement Account Plan')]")
	public WebElement CBplantitle;
	
	@FindBy(xpath = "//h4[contains(text(),'How It Works')]")
	public WebElement Howitworkstitle;
	
	@FindBy(xpath = "//*[@id='howitwork']/p/span/ul")
	public WebElement Howitworkcontent;
	
	@FindBy(xpath = "//span[contains(text(),'Contact Us')]")
	public WebElement ContactUslink;
	
	@FindBy(xpath = "//h1[contains(text(),'Contact Us')]")
	public WebElement ContactUspagetitle;
	
	@FindBy(xpath = "//span[contains(text(),'Back')]")
	public WebElement CUBackbutton;
	
	//-------------------------------------------Right Rail Elements-------------------------------------------//
	
	//li[@class='congratulations-box ng-scope']
	@FindBy(xpath = "//ul[@class='right-box']/li[1]")
	public WebElement RightRailPensionBox;
	
	@FindBy(xpath = "//ul[@class='right-box']/li[1]/h3/span")
	public WebElement RightRailPensionTitle;
	
	@FindBy(xpath = "//ul[@class='right-box']/li[1]/p/span")
	public WebElement RightRailPensionText;
	
	@FindBy(xpath = "//ul[@class='right-box']/li[1]/div/span/a")
	public WebElement RightRailPensionButton;
	
	@FindBy(xpath = "//ul[@class='right-box']/li[2]")
	public WebElement RightRailEstimateBox;
	
	@FindBy(xpath = "//ul[@class='right-box']/li[2]/h4/span")
	public WebElement RightRailEstimateTitle;
	
	@FindBy(xpath = "//ul[@class='right-box']/li[2]/p/span")
	public WebElement RightRailEstimateText;
	
	@FindBy(xpath = "//ul[@class='right-box']/li[2]/div/span/a")
	public WebElement RightRailEstimateButton;
	
	
	//--------------------------------------------BS Hybrid Methods--------------------------------------------//
	
	public void VerifyHybPlanSummary(ExtentTest test) throws InterruptedException {
		waitForElementToDisplay(HybPlanSummaryLabel);
		verifyElementTextContains(HybPlanSummaryLabel, "Plan Summary", test);
		verifyElementIsPresent(ViewHistoryLink, test, "View History Link");
		verifyElementIsPresent(BalanceHistoryGraph, test, "Balance History Detail Graph");
		verifyElementIsPresent(APDetailsCnt, test, "Active Portion Details");
		verifyElementIsPresent(FPDetailsCnt, test, "Frozen Portion Details");
		verifyElementIsPresent(ImpDatesDetailsCnt, test, "Important Dates Content");
	}
	
	//-----------------------------------------Plan Summary CB Methods-----------------------------------------//
	
	public void VerifyPlanSummary(ExtentTest test) throws InterruptedException {
		waitForElementToDisplay(YourPlanSummaryLabel);
		verifyElementTextContains(YourPlanSummaryLabel, "Your Plan Summary", test);
		verifyElementIsPresent(ViewHistoryLink, test, "View History Link");
		verifyElementIsPresent(APDetailsCnt, test, "Active Portion Details");
		verifyElementIsPresent(BalanceHistoryGraph, test, "Balance History Detail Graph");
		verifyElementIsPresent(ImpDatesDetailsCnt, test, "Important Dates Content");
		verifyElementTextContains(Howitworkstitle, "How It Works", test);
		verifyElementIsPresent(Howitworkcontent, test, "How it works Content");
		verifyElementTextContains(ContactUslink, "Contact Us", test);
		clickElement(ContactUslink);
		waitForElementToDisplay(ContactUspagetitle);
		verifyElementTextContains(ContactUspagetitle, "Contact Us", test);
		verifyElementIsPresent(CUBackbutton, test, "Contact Us Back Button");
		clickElement(CUBackbutton);
		waitForElementToDisplay(YourPlanSummaryLabel);
		verifyElementTextContains(YourPlanSummaryLabel, "Your Plan Summary", test);
	}
	
	//--------------------------------------------Right Rail Methods------------------------------------------//
	
	public void VerifyRightRailPension(ExtentTest test) throws InterruptedException {
		verifyElementIsPresent(RightRailPensionBox, test, "Right Rail Commencement Details Box");
		String Text = RightRailPensionButton.getText();
		if(Text == "Get Started") {
			verifyElementTextContains(RightRailPensionTitle, "Your Pension Benefit", test);
			verifyElementTextContains(RightRailPensionText, "You are now eligible to begin receiving your pension benefit.", test);
		}else if(Text == "View Status"){
			verifyElementTextContains(RightRailPensionTitle, "Complete Benefit Commencement", test);
			verifyElementTextContains(RightRailPensionText, "View the status of your benefit commencement request to receive your pension benefit.", test);
		}
		clickElement(RightRailPensionButton);
		
		ROL_Page rol = new ROL_Page(driver);
		waitForElementToDisplay(rol.BenefitCommOverviewLabel);
		verifyElementIsPresent(rol.BenefitCommencementLabel, test, "Benefit Commencement Label");
		verifyElementIsPresent(rol.BenefitCommOverviewLabel, test, "Benefit Commencement Overview Label");
		driver.navigate().back();
	}
	
	public void VerifyRightRailEstimate(ExtentTest test, String flow) throws InterruptedException {
		verifyElementIsPresent(RightRailEstimateBox, test, "Right Rail Estimate Details Box");
		String Text = RightRailEstimateText.getText();
		clickElement(RightRailEstimateButton);
		if(Text.contains("You have not yet created a pension estimate.") || Text.contains("You don't have any recent estimates.")) {
			if(flow=="OverviewPage") {
				waitForElementToDisplay(EstimateYourPensionLabel);
				verifyElementIsPresent(EstimateYourPensionLink, test, "Estimate Your Pension Link");
				verifyElementIsPresent(EstimateYourPensionLabel, test, "Estimate Your Pension Label");
				driver.navigate().back();
			}else if(flow=="EstimatePage") {
				waitForElementToDisplay(EstimateYourPensionLabel);
				verifyElementIsPresent(EstimateYourPensionLink, test, "Estimate Your Pension Link");
				verifyElementIsPresent(EstimateYourPensionLabel, test, "Estimate Your Pension Label");
			}
		}else if(Text.contains("You generated an estimate")) {
			if(flow=="OverviewPage") {
				waitForElementToDisplay(EstimateDetailsLabel);
				verifyElementIsPresent(MostRecentEstimateLink, test, "Most Recent Estimate Link");
				verifyElementIsPresent(EstimateDetailsLabel, test, "Estimate Details Label");
				driver.navigate().back();
			}else if(flow=="EstimatePage") {
				waitForElementToDisplay(EstimateDetailsLabel);
				verifyElementIsPresent(MostRecentEstimateLink, test, "Most Recent Estimate Link");
				verifyElementIsPresent(EstimateDetailsLabel, test, "Estimate Details Label");
			}
		}
	}
	
	
	//---------------------------------------------Estimate Methods--------------------------------------------//
	
	public void Estimatecalc(ExtentTest test, String flow)  throws InterruptedException {
		clickElement(Normalretirementradiobutton);
		String DS_Text = DefaultStatus.getText();
		System.out.println(DS_Text);
		if(flow == "RE") {
			if(DS_Text == "Single") {
				BeneficiaryDOB.sendKeys(ROL_Page.CurDateManipulator(2, 5, -30));	
			} 
			else {
				clickElement(SelectStatus(driver, "Single"));
				BeneficiaryDOB.sendKeys(ROL_Page.CurDateManipulator(2, 5, -30));
			}
		}else if(flow == "RDP") {
			if(DS_Text == "Registered Domestic Partner") {
				verifyElementTextContains(RPD_Dob_Label, "Registered Domestic Partner Date of Birth", test);
				RDP_DOB.sendKeys(ROL_Page.CurDateManipulator(2, 5, -30));	
			} 
			else {
				clickElement(SelectStatus(driver, "Registered Domestic Partner"));
				verifyElementTextContains(RPD_Dob_Label, "Registered Domestic Partner Date of Birth", test);
				RDP_DOB.sendKeys(ROL_Page.CurDateManipulator(2, 5, -30));
			}
		}
		clickElement(Continuebutton);
		verifyElementTextContains(AcknowledgementHeader,"Please Read This Important Information",test);
		clickElement(Acknowledgementcheckbox);
		clickElement(Acceptbutton);
		waitForElementToDisplay(calculateestimatelabel);
		verifyElementTextContains(calculateestimatelabel,"Calculating Your Estimate",test);
		waitForElementToDisappear(By.xpath("//span[contains(text(),'Calculating Your Estimate')]"));
		if (EstimateCalculatedLabel.getText().equals("Your Estimate Has Been Calculated"))
		{
			waitForElementToDisplay(Viewresultsbutton);
			clickElement(Viewresultsbutton);
			verifyElementTextContains(Estimatedetailslabel,"Estimate Details",test);
			verifyElementTextContains(SLALabel,"Single Life Annuity",test);
			verifyElementTextContains(AssumptionLabel,"Assumptions Used for Displayed Pension Estimates",test);
			verifyElementTextContains(calculateAnotherestimatebutton,"Calculate Another Estimate",test);
			
		}
		else if (EstimateCalculatedErrorLabel.getText().equals("Calculation Error")){
			waitForElementToDisplay(EstimateCalculatedErrorLabel);
			verifyElementTextContains(EstimateCalculatedErrorLabel,"Calculation Error",test);
			System.out.println("Estimate Calc Failed");
		}
	}
	
	public void EstimatecalcCSO(ExtentTest test)  throws InterruptedException {
		clickElement(Normalretirementradiobutton);
		//waitForElementToDisplay(Singlemaritalstatus);
		BeneficiaryDOB.sendKeys(ROL_Page.CurDateManipulator(2, 5, -30));
		clickElement(Continuebutton);
		verifyElementTextContains(AcknowledgementHeader,"Please Read This Important Information",test);
		clickElement(Acknowledgementcheckbox);
		clickElement(Acceptbutton);
		waitForElementToDisplay(calculateestimatelabel);
		verifyElementTextContains(calculateestimatelabel,"Calculating Your Estimate",test);
		waitForElementToDisappear(By.xpath("//span[contains(text(),'Calculating Your Estimate')]"));
		if (EstimateCalculatedLabel.getText().equals("Your Estimate Has Been Calculated"))
		{
			waitForElementToDisplay(Viewresultsbutton);
			clickElement(Viewresultsbutton);
			verifyElementTextContains(Estimatedetailslabel,"Estimate Details",test);
			verifyElementTextContains(SLALabel,"Single Life Annuity",test);
			verifyElementTextContains(AssumptionLabel,"Assumptions Used for Displayed Pension Estimates",test);
			verifyElementTextContains(calculateAnotherestimatebutton,"Calculate Another Estimate",test);
			
		}
		else if (EstimateCalculatedErrorLabel.getText().equals("Calculation Error")){
			waitForElementToDisplay(EstimateCalculatedErrorLabel);
			verifyElementTextContains(EstimateCalculatedErrorLabel,"Calculation Error",test);
			System.out.println("Estimate Calc Failed");
		}
	}
		
	//------------------------------------------Estimate Modeling Elements-----------------------------------------//
	
	@FindBy(css = "div[ng-if='!estimate.IsRedCalc']")
	public WebElement EstModelDetails;
	
	@FindBy(xpath = "//span[@class='estimate-name ng-binding']")
	public WebElement EstimateName;
	
	@FindBy(xpath = "(//td[@class='text-middle text-center col-lg-1 col-md-1 print-hide ng-scope'])[10]")
	public WebElement CheckBox50;
	
	@FindBy(xpath = "(//td[@class='text-middle text-center col-lg-1 col-md-1 print-hide ng-scope'])[12]")
	public WebElement CheckBox100;
	
	@FindBy(xpath = "(//td[@class='text-middle text-center col-lg-1 col-md-1 print-hide ng-scope'])[10]//following::td")
	public WebElement FOP50;
	
	@FindBy(xpath = "(//td[@class='text-middle text-center col-lg-1 col-md-1 print-hide ng-scope'])[12]//following::td")
	public WebElement FOP100;
	
	@FindBy(xpath = "//span[contains(text(),'Model These Options')]")
	public WebElement ModelButton;
	
	@FindBy(xpath = "//h2[contains(text(),'Model Payment Options')]")
	public WebElement MPOLabel;
	
	@FindBy(xpath = "(//div[@class='tooltip-inner'])[1]")
	public WebElement RetrieeTooltip;
	
	@FindBy(xpath = "(//div[@class='tooltip-inner'])[2]")
	public WebElement BeneTooltip;
	
	@FindBy(xpath = "(//h5[@class='ng-binding'])[1]")
	public WebElement FOPHeader1;
	
	@FindBy(xpath = "(//h5[@class='ng-binding'])[2]")
	public WebElement FOPHeader2;
	
	@FindBy(css = "#highcharts-0 > svg > g.highcharts-series-group > g:nth-child(1) > path.highcharts-tracker")
	public WebElement RtrBar50;
	
	@FindBy(css = "#highcharts-0 > svg > g.highcharts-series-group > g:nth-child(3) > path.highcharts-tracker") 
	public WebElement BeneBar50;
	
	@FindBy(css = "#highcharts-2 > svg > g.highcharts-series-group > g:nth-child(1) > path.highcharts-tracker")
	public WebElement RtrBar100;
	
	@FindBy(css = "#highcharts-2 > svg > g.highcharts-series-group > g:nth-child(3) > path.highcharts-tracker") 
	public WebElement BeneBar100;
	
	@FindBy(xpath = "//div[@class='popover-content']")
	public WebElement PopUpContent;
	
	@FindBy(xpath = "//span[@id='close']/span[@class='icomn-close']")
	public WebElement PopUpClose;
	
	@FindBy(xpath = "//div[@class='popover-content']/div/div[2]/span[2]")
	public WebElement PopUpAmount;
	
	//------------------------------------------Estimate Modeling Methods-----------------------------------------//
	
	public void EstimateModeling(WebDriver driver, ExtentTest test)  throws InterruptedException {
		waitForElementToDisplay(EstimateName);
		verifyElementIsPresent(EstModelDetails, test, "Details");
		verifyElementIsPresent(EstimateName, test, "Estimate Name");
		String estName = EstimateName.getText();
		clickElement(CheckBox50);
		String fopheader1Name = FOP50.getText();
		//waitForElementToDisplay(CheckBox100);
		//clickElementUsingJavaScript(driver, CheckBox100);
		//String fopheader2Name = FOP100.getText();
				
		waitForElementToDisplay(ModelButton);
		clickElement(ModelButton);
		
		waitForElementToDisplay(MPOLabel);
		verifyElementTextContains(MPOLabel, "Model Payment Options", test);
		verifyElementTextContains(EstimateName, estName, test);
		verifyElementIsPresent(RetrieeTooltip, test, "Retriee Tooptip Age");
		verifyElementIsPresent(BeneTooltip, test, "Beneficiary Tooptip Age");
		waitForElementToDisplay(FOPHeader1);
		verifyElementTextContains(FOPHeader1, fopheader1Name, test);
		//verifyElementTextContains(FOPHeader2, fopheader2Name, test);
		
		doubleClickElement(driver, RtrBar50);
		waitForElementToDisplay(PopUpContent);
		verifyElementIsPresent(PopUpContent, test, fopheader1Name+" Retriee Popup Content");
		String R50_Amt = PopUpAmount.getText().substring(1);
		R50_Amt = R50_Amt.replaceAll(",", "");
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		Double am = (Double.parseDouble(R50_Amt))/2;
		String amt = twoDForm.format(am);
		clickElement(PopUpClose);
		doubleClickElement(driver, BeneBar50);
		waitForElementToDisplay(PopUpContent);
		verifyElementIsPresent(PopUpContent, test, fopheader1Name+" Retriee Popup Content");
		String B50_Amt = PopUpAmount.getText().substring(1);
		B50_Amt = B50_Amt.replaceAll(",", "");
		clickElement(PopUpClose);
		verifyEquals(B50_Amt, amt, test);
		
		/*
		 * doubleClickElement(driver, RtrBar100); waitForElementToDisplay(PopUpContent);
		 * verifyElementIsPresent(PopUpContent, test,
		 * fopheader1Name+" Retriee Popup Content"); String R100_Amt =
		 * PopUpAmount.getText(); clickElement(PopUpClose); doubleClickElement(driver,
		 * BeneBar100); waitForElementToDisplay(PopUpContent);
		 * verifyElementIsPresent(PopUpContent, test,
		 * fopheader1Name+" Retriee Popup Content"); String B100_Amt =
		 * PopUpAmount.getText(); clickElement(PopUpClose); verifyEquals(R100_Amt,
		 * B100_Amt, test);
		 */
		
	}
	
	//-----------------------------------------------My Data Elements--------------------------------------------//
	
	@FindBy(xpath = "//span[contains(text(),'Pay History')]")
	public WebElement PayHistorySection;
	
	@FindBy(xpath = "//div[contains(text(),'Year')]")
	public WebElement PayHistoryYearColumn;
	
	@FindBy(xpath = "//div[contains(text(),'Base Pay')]")
	public WebElement PayHistoryBasePayColumn;
	
	@FindBy(xpath = "//div[contains(text(),'Other')]")
	public WebElement PayHistoryOtherColumn;
	
	@FindBy(xpath = "//div[contains(text(),'Total')]")
	public WebElement PayHistoryTotalColumn;
	
	@FindBy(xpath = "//div[contains(text(),'Credited Service Hours')]")
	public WebElement PayHistoryCreditedHrsColumn;
	
	@FindBy(xpath = "//div[contains(text(),'Vested Hours')]")
	public WebElement PayHistoryVestedHrsColumn;
	
	@FindBy(xpath = "//span[contains(text(),'Employment History')]")
	public WebElement EmpHistorySection;
	
	@FindBy(xpath = "//div[contains(text(),'Effective Date')]")
	public WebElement EmpHistoryEffDateColumn;
	
	@FindBy(xpath = "//div[contains(text(),'Employment Status')]")
	public WebElement EmpHistoryEmpStatusColumn;
	
	@FindBy(xpath = "//div[contains(text(),'Description')]")
	public WebElement EmpHistoryDescriptionColumn;
	
	@FindBy(xpath = "//span[contains(text(),'Plan Specific Data')]")
	public WebElement PlanSpecificDataSection;
	
	@FindBy(xpath = "//i[@class='fa fa-chevron-circle-up']")
	public WebElement HideButton;
	
	@FindBy(xpath = "(//i[@class='fa fa-chevron-circle-down'])[2]")
	public WebElement ExpandButton1;
	
	@FindBy(xpath = "(//i[@class='fa fa-chevron-circle-down'])[3]")
	public WebElement ExpandButton2;
	
	@FindBy(xpath = "//h4[contains(text(),'Vesting Service')]")
	public WebElement VestingSvsTile;
	
	//-----------------------------------------------My Data  Methods--------------------------------------------//
	
	public void myDataFlow(ExtentTest test) throws InterruptedException {
		waitForElementToDisplay(PayHistorySection);
		verifyElementTextContains(PayHistorySection, "Pay History", test);
		verifyElementIsPresent(PayHistoryYearColumn, test, "Year Column");
		verifyElementIsPresent(PayHistoryBasePayColumn, test, "BasePay Column");
		verifyElementIsPresent(PayHistoryOtherColumn, test, "Other Column");
		verifyElementIsPresent(PayHistoryTotalColumn, test, "Total Column");
		verifyElementIsPresent(PayHistoryCreditedHrsColumn, test, "CreditedHrs Column");
		verifyElementIsPresent(PayHistoryVestedHrsColumn, test, "VestedHrs Column");
		clickElement(HideButton);
		
		verifyElementTextContains(EmpHistorySection, "Employment History", test);
		clickElement(ExpandButton1);
		waitForElementToDisplay(EmpHistoryEffDateColumn);
		verifyElementIsPresent(EmpHistoryEffDateColumn, test, "EffDate Column");
		verifyElementIsPresent(EmpHistoryEmpStatusColumn, test, "EmpStatus Column");
		verifyElementIsPresent(EmpHistoryDescriptionColumn, test, "Description Column");
		clickElement(HideButton);
		
		/*
		 * verifyElementTextContains(PlanSpecificDataSection, "Plan Specific Data",
		 * test); clickElement(ExpandButton2); waitForElementToDisplay(VestingSvsTile);
		 * verifyElementIsPresent(VestingSvsTile, test, "VestingSvs Tile");
		 */
	}
	
}

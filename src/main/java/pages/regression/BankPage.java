package pages.regression;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BankPage {
	WebDriver driver;

	public BankPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
		//--------------------------------------------------------Bank Elements--------------------------------------------------------------//
		
		//--------------------------------------------------Payment Elements --------------------------------------------------//
		@FindBy(xpath = "//button/span[contains(text(),'Edit')]")
		public WebElement PaymentMethodEdit;
		
		@FindBy(xpath = "//h3[contains(text(),'Edit Payment Method')]")
		public WebElement EditPaymentMethodHeader;
		
		@FindBy(xpath = "(//button[@type='button'][@class='btn btn-default'])[1]")
		public WebElement PaymentMethodDropdown;
		
		@FindBy(xpath = "(//button[@type='button'][@class='btn btn-default'])[2]")
		public WebElement AccountDropdown;
		
		public WebElement PaymentMethodDropdownOption(WebDriver driver, String Option) {
			WebElement PaymentMethodDropdownOption = driver.findElement(By.xpath("//a[@role='menuitem'][contains(text(),'"+ Option +"')]"));
			return PaymentMethodDropdownOption;
		}
		
		@FindBy(xpath = "//span[contains(text(),'Your changes have not been saved.')]")
		public WebElement SuccessfulPMEditMsg;
		
		@FindBy(xpath = "//span[@class='ng-scope'][contains(text(),'Add a Bank Account')]")
		public WebElement AddBankAccountButton;
		
		@FindBy(css = "a[href='/my-account/bank-accounts']")
		public WebElement BankAccountslink;
		
		@FindBy(xpath = "//h3[contains(text(),'Add a Bank Account')]")
		public WebElement AddBankAccountHeader;
		
		@FindBy(xpath = "//input[@name='routingNumber']")
		public WebElement RoutingNumber;
		
		@FindBy(xpath = "//input[@name='routingNumberConfirm']")
		public WebElement ConfirmRoutingNumber;
		
		@FindBy(xpath = "//input[@name='accountNumber']")
		public WebElement AccountNumber;
		
		@FindBy(xpath = "//input[@name='accountNumberConfirm']")
		public WebElement ConfirmAccountNumber;
		
		@FindBy(xpath = "//input[@name='accountName']")
		public WebElement AccountName;
		
		@FindBy(xpath = "//label[@for='bankAccountCheckbox']")
		public WebElement CheckBoxdisclaimer;
		
		@FindBy(xpath = "//button[contains(text(),'Add')]")
		public WebElement AddBankButton;
		
		@FindBy(xpath = "//button[contains(text(),'Cancel')]")
		public WebElement CancelBankButton;
		
		@FindBy(xpath = "(//button[contains(text(),'OK')])[2]")
		public WebElement OKBankButton;
		
		@FindBy(xpath = "//button[contains(text(),'Save')][@class='btn btn-primary']")
		public WebElement BankSaveButton;
		
		@FindBy(xpath = "//div[@ng-show='displayPopup']/div/div/button")
		public WebElement BankAccPopupOkBtn;
		
		//--------------------------------------------------Bank Account--------------------------------------------------//
		@FindBy(xpath = "//span[contains(text(),'Bank Accounts')]")
		public WebElement BankAccountHeader;
		
		@FindBy(xpath = "(//span[contains(text(),'Edit')])[5]")
		public WebElement EditBankButton;
		
		@FindBy(xpath = "(//input[@id='accountName'])[1]")
		public WebElement AccountName1;
		
		@FindBy(xpath = "//button[contains(text(),'Save')]")
		public WebElement BankEditSaveButton;
		
		@FindBy(xpath = "//span[contains(text(),'Your changes have been saved.')]")
		public WebElement SuccessfulEditMsg;
		
		@FindBy(xpath = "(//span[contains(text(),'Delete')])[5]")
		public WebElement DeleteBankButton;
		
		@FindBy(xpath = "//div[@ng-if='showDeleteConfirm']/div/div/button[contains(text(),'Delete')]")
		public WebElement ConfirmDeleteButton;
		
		@FindBy(xpath = "//span[contains(text(),'You have successfully deleted an unassigned bank account.')]")
		public WebElement SuccessfulDeleteMsg;
		
		
		//--------------------------------------------------Bank Functions--------------------------------------------------//
		
		public void editPaymentMethod(WebDriver driver) throws InterruptedException {
			waitForElementToDisplay(PaymentMethodDropdown);
			clickElement(PaymentMethodDropdown);
			clickElement(PaymentMethodDropdownOption(driver, "Direct Deposit"));
			clickElement(AccountDropdown);
			clickElement(PaymentMethodDropdownOption(driver, "BOB bank X0147"));
			waitForElementToDisplay(OKBankButton);
			clickElement(OKBankButton);
		}
		
		public void addBankAccount() throws InterruptedException {
			setInput(RoutingNumber,"071000013");
			setInput(ConfirmRoutingNumber,"071000013");
			setInput(AccountNumber,"13579");
			setInput(ConfirmAccountNumber,"13579");
			setInput(AccountName,"TestBank");
			clickElement(CheckBoxdisclaimer);
			waitForElementToDisplay(AddBankButton);
			clickElement(AddBankButton);
			waitForElementToDisplay(BankAccPopupOkBtn);
			clickElement(BankAccPopupOkBtn);
		}
		
		public void editBankAccount() throws InterruptedException {
			waitForElementToDisplay(EditBankButton);
			clickElement(EditBankButton);
			setInput(AccountName1, "Test Bank Updated");
			waitForElementToDisplay(BankEditSaveButton);
			clickElement(BankEditSaveButton);
		}
		
		public void deleteBankAccount() throws InterruptedException {
			waitForElementToDisplay(DeleteBankButton);
			clickElement(DeleteBankButton);
			waitForElementToDisplay(ConfirmDeleteButton);
			clickElement(ConfirmDeleteButton);
		}
}

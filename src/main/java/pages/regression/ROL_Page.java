package pages.regression;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.delay;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

public class ROL_Page {
	WebDriver driver;
	private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	public ROL_Page(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	
	//------------------------------------------FP Elements------------------------------------------//	
	
	@FindBy(xpath = "//h2[contains(text(),'Benefit Commencement')]")
	public WebElement BenefitCommencementLabel;
	
	@FindBy(xpath = "//h2[contains(text(),'Benefit Commencement Overview')]")
	public WebElement BenefitCommOverviewLabel;
	
	@FindBy(css = "button[ng-click*='startCommencement']")
	public WebElement GetStartedROLBtn;
	
	@FindBy(xpath = "//span[contains(text(),'My acknowledgement below certifies that:')]")
	public WebElement MyAcknwmntLabel;
	
	@FindBy(css = "label[for='agreement']")
	public WebElement AgreementChkBox;
	
	@FindBy(xpath = "//label[@for='agreement']/span")
	public WebElement AgreementChkBoxText;
	
	@FindBy(xpath = "//button[contains(text(),'Accept')]")
	public WebElement AcceptButton;
	
	//--------------------------------------------FP Methods-------------------------------------------//
	
	public void FP_Flow(WebDriver driver, ExtentTest test) throws InterruptedException {
		DashboardPage dashboard = new DashboardPage(driver);
		verifyElementTextContains(dashboard.NotStartedLabel, "Not Started", test);
		waitForElementToDisplay(dashboard.GetStartedBtn);
		clickElement(dashboard.GetStartedBtn);
		waitForElementToDisplay(BenefitCommencementLabel);
		verifyElementTextContains(BenefitCommencementLabel, "Benefit Commencement", test);
		verifyElementTextContains(BenefitCommOverviewLabel, "Benefit Commencement", test);
		waitForElementToDisplay(GetStartedROLBtn);
		verifyElementTextContains(GetStartedROLBtn, "Get Started", test);
		clickElement(GetStartedROLBtn);
		waitForElementToDisplay(MyAcknwmntLabel);
		verifyElementTextContains(MyAcknwmntLabel, "My acknowledgement below certifies that:", test);
		verifyElementIsPresent(AgreementChkBox, test, "Agreement CheckBox");
		verifyElementTextContains(AgreementChkBoxText, "By checking this box I affirmatively consent to receiving the legally required notices for the Benefit Commencement process electronically.", test);
		clickElement(AgreementChkBox);
		waitForElementToDisplay(AcceptButton);	
		verifyElementIsPresent(AcceptButton, test, "Accept Button");
	}
	
	//-------------------------------------------SOC Elements-------------------------------------------//
	
	@FindBy(xpath = "(//button[@ng-click='getStarted()'])[1]")
	public WebElement GetStarted_Btn1;
	
	@FindBy(xpath = "(//button[@ng-click='getStarted()'])[2]")
	public WebElement GetStarted_Btn2;
	
	@FindBy(xpath = "(//button[@ng-click='getStarted()'])[3]")
	public WebElement GetStarted_Btn3;
	
	@FindBy(xpath = "//span[contains(text(),'Resume')]")
	public WebElement ResumeButton;
	
	@FindBy(xpath = "(//button[@ng-click='startCommencement(plan.PlanCode)'])[1]")
	public WebElement GetStartedROL_Btn1;
	
	@FindBy(xpath = "(//button[@ng-click='startCommencement(plan.PlanCode)'])[2]")
	public WebElement GetStartedROL_Btn2;
	
	@FindBy(xpath = "(//button[@ng-click='startCommencement(plan.PlanCode)'])[3]")
	public WebElement GetStartedROL_Btn3;
	
	@FindBy(xpath = "//span[contains(text(),'Qualified Domestic Relations Order')]")
	public WebElement QDRO_Header;
	
	@FindBy(xpath = "//h4[contains(text(),'No DROs/QDROs')]")
	public WebElement No_DROs_QDROs_Label;
	
	@FindBy(xpath = "(//span[contains(text(),'Select')])[1]")
	public WebElement No_DROs_QDROs_Select;
	
	@FindBy(xpath = "//button[@ng-click='nextButtonClick()']")
	public WebElement NextButton;
	
	@FindBy(xpath = "//button[@ng-click='prevButtonClick()']")
	public WebElement PreviousButton;
	
	@FindBy(xpath = "//span[contains(text(),'Benefit Commencement Details')]")
	public WebElement BC_Details_Header;
	
	@FindBy(xpath = "//span[contains(text(),'Benefit Commencement Date')]")
	public WebElement BC_Dates_Label;
	
	@FindBy(xpath = "//button[@title='Year']")
	public WebElement YearDropdown;
	
	public WebElement YearDropdownOption(WebDriver driver, String Option) {
		WebElement YearDropdownOption = driver.findElement(By.xpath("//a/span[contains(text(),'"+ Option +"')]"));
		return YearDropdownOption;
	}
	
	@FindBy(xpath = "//button[@title='Month']")
	public WebElement MonthDropdown;
	
	public WebElement MonthDropdownOption(WebDriver driver, String Option) {
		WebElement monthDropdownOption = driver.findElement(By.xpath("//a/span[contains(text(),'"+ Option +"')]"));
		return monthDropdownOption;
	}
	
	@FindBy(xpath = "(//label/span[contains(text(),'Age')])[1]")
	public WebElement AgeLabel;
	
	@FindBy(xpath = "//span[contains(text(),'Joint & Survivor Beneficiary')]")
	public WebElement JnS_Beneficiary_Label;
	
	@FindBy(xpath = "//label/span[contains(text(),'No')]")
	public WebElement NoBenOption;
	
	@FindBy(xpath = "//button/span[contains(text(),'Choose Beneficiary')]")
	public WebElement ChooseBeneDrpDwn;
	
	@FindBy(xpath = "//a[contains(text(),'No Beneficiary')]")
	public WebElement NoBene_DD_Option;
	
	@FindBy(xpath = "//h2[contains(text(),'Please Read This Important Information')]")
	public WebElement PRTII_Header;
	
	@FindBy(xpath = "//*[@id=\"themeClassPlace\"]/body/div[3]/div[3]/div/div[1]/div[1]/div/div[2]/div[1]/div/div[2]/div/div/h2/span[1]")
	public WebElement LoaderImg;

	@FindBy(xpath = "//span[contains(text(),'Your Payment Options Have Been Calculated')]")
	public WebElement YPOHBC_Header;
	
	@FindBy(xpath = "//button[contains(text(), 'View Results')]")
	public WebElement ViewResultsBtn;
	
	@FindBy(xpath = "//span[contains(text(),'Select Payment Type')]")
	public WebElement SelectPaymentTypeHeader;
	
	@FindBy(xpath = "((//tr)[5]/td)[1]")
	public WebElement SingleLifeAnnunityPaymentType;
	
	@FindBy(xpath = "//span[contains(text(),'Additional Benefits')]")
	public WebElement AdditionalBenefitsHeader;
	
	@FindBy(xpath = "//h3[contains(text(),'Supplemental Benefits')]")
	public WebElement SupplementalBenefitsLabel;
	
	@FindBy(xpath = "//span[contains(text(),'Summary of Selected Payment Type')]")
	public WebElement SummaryofSelectedPaymentTypeHeader;
	
	@FindBy(xpath = "//h3[contains(text(),'Single Life Annuity')]")
	public WebElement SingleLifeAnnunityLabel;
	
	@FindBy(xpath = "//span[contains(text(),'Post-Retirement')]")
	public WebElement PostRetirementBeneInfoHeader;
	
	@FindBy(xpath = "//div[@class='beneficiaries ng-scope']")
	public WebElement PostRetirementDetails;
	
	@FindBy(xpath = "//span[contains(text(),'Payment Method')]")
	public WebElement PaymentMethodHeader;
	
	@FindBy(xpath = "//div[contains(text(),'Check will be mailed to your Primary address')]")
	public WebElement CheckSelectedMsg;
	
	@FindBy(css = "label[for='annuityAgreement']")
	public WebElement DirectDepositAgreementChkBox;
	
	@FindBy(xpath = "//label[@for='annuityAgreement']/span")
	public WebElement DirectDepositAgreementChkBoxText;
	
	@FindBy(xpath = "//span[contains(text(),'Tax Withholding Elections')]")
	public WebElement TaxWithholdingElectionsHeader;
	
	@FindBy(xpath = "(//div[@class='checkbox checkbox-success ng-scope']/label/span)[1]")
	public WebElement FedTaxcheckbox1;
	
	@FindBy(xpath = "(//div[@class='checkbox checkbox-success ng-scope']/label/span)[2]")
	public WebElement MicTaxcheckbox2;
	
	@FindBy(xpath = "(//span[contains(text(),'Yes')])[1]")
	public WebElement MicWithholdingDropdown;
	
	@FindBy(xpath = "(//a[contains(text(),'No')])[2]")
	public WebElement MicWithholdingNoOption;
	
	@FindBy(xpath = "(//span[contains(text(),'Required Documents')])[2]")
	public WebElement RequiredDocumentsHeader;
	
	@FindBy(xpath = "//span[contains(text(),'Document Upload')]")
	public WebElement DocumentUploadLabel;
	
	@FindBy(xpath = "//div[@class='upload-control']/div/div[3]/a")
	public WebElement SelectFileBtn;
	
	@FindBy(xpath = "//a[@class='fake-check']")
	public WebElement DocChk;
	
	public WebElement DocChkBox(WebDriver driver, int ChkNo) {
		WebElement DocChkBox = driver.findElement(By.xpath("(//a[@class='fake-check']/i)["+ ChkNo +"]"));
		return DocChkBox;
	}
	
	@FindBy(xpath = "//button[contains(text(),'Add Another File')]")
	public WebElement AddAnotherFileBtn;
	
	@FindBy(xpath = "//div[@class='upload-control']/div/div[3]/a")
	public WebElement UploadFileBtn;
	
	public WebElement DocStatus(WebDriver driver, String DocNo) {
		WebElement DocStat = driver.findElement(By.xpath("(//span[@ng-bind='document.getStatus().Title'])["+ DocNo +"]"));
		return DocStat;
	}
	
	@FindBy(xpath = "//span[contains(text(),'Review Your Information')]")
	public WebElement ReviewYourInformationHeader;
	
	@FindBy(xpath = "//div[@class='container padding-left rol-section']")
	public WebElement ReviewYourInfoPageContent;
	
	@FindBy(xpath = "//span[contains(text(),'Waivers and Notices')]")
	public WebElement WaiversandNoticesHeader;
	
	@FindBy(xpath = "//label[@for='ch_0']/span/p")
	public WebElement WaiversandNoticecheckbox1;
	
	@FindBy(xpath = "//label[@for='ch_1']/span")
	public WebElement WaiversandNoticecheckbox2;
	
	@FindBy(xpath = "//span[contains(text(),'Consents')]")
	public WebElement ConsentsHeader;
	
	@FindBy(xpath = "//label[@for='agreement']")
	public WebElement ConsentChkBx;
	
	@FindBy(xpath = "//span[contains(text(),'SUBMIT BENEFIT COMMENCEMENT')]")
	public WebElement SUBMITBENEFITCOMMENCEMENT_Button;
	
	@FindBy(xpath = "//div[@class='pop-up pop-consents']")
	public WebElement SubmitBenefitCommPopup;
	
	@FindBy(xpath = "//button[contains(text(),'SUBMIT BENEFIT COMMENCEMENT')]")
	public WebElement SUBMITBENEFITCOMMENCEMENT_PopupButton;
	
	@FindBy(xpath = "(//span[contains(text(),'Confirmation')])[1]")
	public WebElement ConfirmationHeader;
	
	@FindBy(xpath = "(//div[@class='ng-scope']/div[@class='ng-scope'])[2]")
	public WebElement ConfirmationPageContent;
	
	//--------------------------------------------SOC Methods-------------------------------------------//
	
	public void SOC_FP_Flow(WebDriver driver, String flow, ExtentTest test) throws InterruptedException {		
		if(flow == "NQ" || flow == "SLA") {
			waitForElementToDisplay(GetStarted_Btn1);
			clickElement(GetStarted_Btn1);
		}else if(flow == "") {
			waitForElementToDisplay(GetStarted_Btn2);
			clickElement(GetStarted_Btn2);
		}else if(flow == "") {
			waitForElementToDisplay(GetStarted_Btn3);
			clickElement(GetStarted_Btn3);
		}		
		waitForElementToDisplay(BenefitCommencementLabel);
		verifyElementTextContains(BenefitCommencementLabel, "Benefit Commencement", test);
		verifyElementTextContains(BenefitCommOverviewLabel, "Benefit Commencement", test);
		if(flow == "NQ"  || flow == "SLA") {
			waitForElementToDisplay(GetStartedROL_Btn1);
			verifyElementTextContains(GetStartedROL_Btn1, "Get Started", test);
			clickElement(GetStartedROL_Btn1);
		}else if(flow == "") {
			waitForElementToDisplay(GetStartedROL_Btn2);
			verifyElementTextContains(GetStartedROL_Btn2, "Get Started", test);
			clickElement(GetStartedROL_Btn2);
		}else if(flow == "") {
			waitForElementToDisplay(GetStartedROL_Btn3);
			verifyElementTextContains(GetStartedROL_Btn3, "Get Started", test);
			clickElement(GetStartedROL_Btn3);
		}		
		waitForElementToDisplay(MyAcknwmntLabel);
		verifyElementTextContains(MyAcknwmntLabel, "My acknowledgement below certifies that:", test);
		verifyElementIsPresent(AgreementChkBox, test, "Agreement CheckBox");
		verifyElementTextContains(AgreementChkBoxText, "By checking this box I affirmatively consent to receiving the legally required notices for the Benefit Commencement process electronically.", test);
		clickElement(AgreementChkBox);
		waitForElementToDisplay(AcceptButton);	
		verifyElementIsPresent(AcceptButton, test, "Accept Button");
	}
	
	public void SOC_Step1_Flow(WebDriver driver, ExtentTest test) throws InterruptedException {
		clickElement(AcceptButton);
		waitForElementToDisplay(QDRO_Header);
		verifyElementTextContains(QDRO_Header, "Qualified Domestic Relations Order", test);
		verifyElementIsPresent(No_DROs_QDROs_Label, test, "No DROs/QDROs Label");
		verifyElementIsPresent(No_DROs_QDROs_Select, test, "No DROs/QDROs Select Button");
		clickElement(No_DROs_QDROs_Select);
		waitForElementToDisplay(NextButton);
		clickElement(NextButton);
		waitForElementToDisplay(BC_Details_Header);
		verifyElementTextContains(BC_Details_Header, "Benefit Commencement Details", test);
		verifyElementTextContains(BC_Dates_Label, "Benefit Commencement Date", test);
		if(isElementExisting(driver, DatePickerLDW) && isElementExisting(driver, DatePickerBCD)) {
			DatePickerLDW.sendKeys(CurDateManipulator(-20,0,0));
			DatePickerBCD.sendKeys(CurDateManipulator(2,3,0));
			
		}else if(isElementExisting(driver, DatePickerLDW)) {
			DatePickerLDW.sendKeys(CurDateManipulator(-20,0,0));
		}else {
			clickElement(YearDropdown);
			clickElement(YearDropdownOption(driver, InPayPage.randnum(2030, 2021)));
			clickElement(MonthDropdown);
			clickElement(MonthDropdownOption(driver, "July"));
			waitForElementToDisplay(AgeLabel);
			verifyElementIsPresent(AgeLabel, test, "Age Label");			
		}
		verifyElementTextContains(JnS_Beneficiary_Label, "Joint & Survivor Beneficiary", test);
		if(isElementExisting(driver, ChooseBeneDrpDwn)) {
			clickElement(ChooseBeneDrpDwn);
			waitForElementToDisplay(NoBene_DD_Option);
			clickElement(NoBene_DD_Option);
		}else {
			clickElement(NoBenOption);
		}
		waitForElementToDisplay(NextButton);
		clickElement(NextButton);
		waitForElementToDisplay(PRTII_Header);
		verifyElementTextContains(PRTII_Header, "Please Read This Important Information", test);
		verifyElementIsPresent(AgreementChkBoxText, test, "Agreement CheckBox Text");
		clickElement(AgreementChkBox);
		waitForElementToDisplay(AcceptButton);	
		clickElement(AcceptButton);
		waitForElementToDisplay(LoaderImg);
		waitForElementToDisappear(By.xpath("//*[@id='themeClassPlace']/body/div[3]/div[3]/div/div[1]/div[1]/div/div[2]/div[1]/div/div[2]/div/div/h2/span[1]"));
		delay(10000);
	}
	
	public void SOC_Step2_4_Flow(WebDriver driver, ExtentTest test) throws InterruptedException {
		waitForElementToDisplay(SelectPaymentTypeHeader);
		verifyElementTextContains(SelectPaymentTypeHeader, "Select Payment Type", test);
		clickElement(SingleLifeAnnunityPaymentType);
		waitForElementToDisplay(NextButton);
		clickElement(NextButton);
		if(isElementExisting(driver, AdditionalBenefitsHeader)) {
			verifyElementTextContains(AdditionalBenefitsHeader, "Additional Benefits", test);
			verifyElementTextContains(SupplementalBenefitsLabel, "Supplemental Benefits", test);
			waitForElementToDisplay(NextButton);
			clickElement(NextButton);
		}else if(isElementExisting(driver, SummaryofSelectedPaymentTypeHeader)) {
			waitForElementToDisplay(SingleLifeAnnunityLabel);
		}
		waitForElementToDisplay(SummaryofSelectedPaymentTypeHeader);
		verifyElementTextContains(SummaryofSelectedPaymentTypeHeader, "Summary of Selected Payment Type", test);
		verifyElementIsPresent(SingleLifeAnnunityLabel, test, "SingleLife Annunity Label");
		waitForElementToDisplay(NextButton);
		clickElement(NextButton);
		waitForElementToDisplay(PostRetirementBeneInfoHeader);
		verifyElementTextContains(PostRetirementBeneInfoHeader, "Post-Retirement", test);
		verifyElementIsPresent(PostRetirementDetails, test, "Post Retirement Details");
		waitForElementToDisplay(NextButton);
		clickElement(NextButton);
		waitForElementToDisplay(PaymentMethodHeader);
		verifyElementTextContains(PaymentMethodHeader, "Payment Method", test);
		BankPage BP = new BankPage(driver);
		clickElement(BP.PaymentMethodDropdown);
		clickElement(BP.PaymentMethodDropdownOption(driver, "Direct Deposit"));
		waitForElementToDisplay(BP.AddBankAccountButton);
		verifyElementIsPresent(BP.AddBankAccountButton, test, "Add Bank Account Button");
		clickElement(DirectDepositAgreementChkBox);
		verifyElementIsPresent(DirectDepositAgreementChkBoxText, test, "Direct Deposit Agreement ChkBox Text");
		waitForElementToDisplay(NextButton);
		clickElement(BP.PaymentMethodDropdown);
		clickElement(BP.PaymentMethodDropdownOption(driver, "Check"));
		waitForElementToDisplay(CheckSelectedMsg);
		verifyElementIsPresent(CheckSelectedMsg, test, "Check Selected Msg");
		waitForElementToDisplay(NextButton);
		clickElement(NextButton);
		waitForElementToDisplay(TaxWithholdingElectionsHeader);
		verifyElementTextContains(TaxWithholdingElectionsHeader, "Tax Withholding Elections", test);
		clickElement(FedTaxcheckbox1);
		if(isElementExisting(driver, MicTaxcheckbox2)) {
			if(isElementExisting(driver, MicWithholdingDropdown)) {
				clickElement(MicWithholdingDropdown);
				clickElement(MicWithholdingNoOption);
				clickElement(MicTaxcheckbox2);
			}else {
				clickElement(MicTaxcheckbox2);
			}
		}else {
			waitForElementToDisplay(NextButton);
		}
		clickElement(NextButton);
		waitForElementToDisplay(RequiredDocumentsHeader);
		verifyElementTextContains(RequiredDocumentsHeader, "Required Documents", test);
		verifyElementIsPresent(DocumentUploadLabel, test, "Document Upload Label");
		//List<WebElement> boxes = DocChk.findElements(By.tagName("i"));
		List<WebElement> boxes = driver.findElements(By.xpath("//a[@class='fake-check']/i"));
		int l;
		if (boxes.size()>5) {
			l=6;
		}else {
			l=boxes.size();
		}
		System.out.println(l);
		for (int i = 1; i < l+1; i++)
		{
			waitForElementToDisplay(SelectFileBtn);
			clickElement(SelectFileBtn);
			File file = new File("./src/main/resources/testdata/Test_Document.pdf");
			driver.findElement(By.xpath("//div[@class='upload-control']/div/div[3]/input")).sendKeys(file.getAbsolutePath());
			clickElement(DocChkBox(driver, i));
			if(i<l) {
				waitForElementToDisplay(AddAnotherFileBtn);
				clickElement(AddAnotherFileBtn);				
			}
		}
		waitForElementToDisplay(UploadFileBtn);
		clickElement(UploadFileBtn);
	}
	
	public void SOC_Step5_Flow(WebDriver driver, ExtentTest test) throws InterruptedException {
		if(isElementExisting(driver, RequiredDocumentsHeader)) {
			waitForElementToDisplay(RequiredDocumentsHeader);
			verifyElementTextContains(RequiredDocumentsHeader, "Required Documents", test);
			verifyElementIsPresent(DocumentUploadLabel, test, "Document Upload Label");
			waitForElementToDisplay(NextButton);
			clickElement(NextButton);
		}else if(isElementExisting(driver, ReviewYourInformationHeader)) {
			waitForElementToDisplay(ReviewYourInformationHeader);
		}
		waitForElementToDisplay(ReviewYourInformationHeader);
		verifyElementTextContains(ReviewYourInformationHeader, "Review Your Information", test);
		verifyElementIsPresent(ReviewYourInfoPageContent, test, "Review Your Info Page Content");
		waitForElementToDisplay(NextButton);
		clickElement(NextButton);

		waitForElementToDisplay(WaiversandNoticesHeader);
		verifyElementTextContains(WaiversandNoticesHeader, "Waivers and Notices", test);
		clickElement(WaiversandNoticecheckbox1);
		clickElement(WaiversandNoticecheckbox2);
		waitForElementToDisplay(NextButton);
		clickElement(NextButton);
		 
		waitForElementToDisplay(ConsentsHeader);
		verifyElementTextContains(ConsentsHeader, "Consents", test);
		clickElement(ConsentChkBx);
		clickElement(SUBMITBENEFITCOMMENCEMENT_Button);
		waitForElementToDisplay(SubmitBenefitCommPopup);
		verifyElementIsPresent(SubmitBenefitCommPopup, test, "Submit Benefit Comm Popup");
		clickElement(SUBMITBENEFITCOMMENCEMENT_PopupButton);
		
		waitForElementToDisplay(ConfirmationHeader);
		verifyElementTextContains(ConfirmationHeader, "Confirmation", test);
		verifyElementIsPresent(ConfirmationPageContent, test, "Confirmation Page Content");
	}
	
	//----------------------------------------------Elements--------------------------------------------//
	
	@FindBy(xpath = "//span[contains(text(),'Status Details')]")
	public WebElement StatusDetailsLink;
	
	@FindBy(css = "h2[data-bo-bind='pageHeader']")
	public WebElement BenCommStepsLabel;
	
	@FindBy(css = "div.gray-back > div:nth-child(1) > table > tbody > tr > td:nth-child(1)")
	public WebElement StepOneLabel;
	
	@FindBy(css = "div.gray-back > div:nth-child(2) > table > tbody > tr > td:nth-child(1)")
	public WebElement StepTwoLabel;
	
	@FindBy(css = "div.gray-back > div:nth-child(3) > table > tbody > tr > td:nth-child(1)")
	public WebElement StepThreeLabel;
	
	@FindBy(css = "a[ng-click='backToOverview()']")
	public WebElement BackOVBtn;
	
	@FindBy(xpath = "//span[contains(text(),'Get Started')]")
	public WebElement GetStartedRKBtn;
	
	@FindBy(xpath = "//h2[contains(text(),'Request a Benefit Commencement Kit')]")
	public WebElement ReqBenCommKitLabel;
	
	@FindBy(xpath = "//span[contains(text(),'Last Day Worked')]")
	public WebElement LstDayWrkdLabel;
	
	@FindBy(xpath = "(//input[@name='dtPickerCurrentDate'])[1]")
	public WebElement DatePickerLDW;
	
	@FindBy(xpath = "(//input[@name='dtPickerCurrentDate'])[2]")
	public WebElement DatePickerBCD;
	
	@FindBy(xpath = "//a[@class='unmarried fake-radio']/span/i[1]")
	public WebElement JSBeneNoRadio;
	
	@FindBy(xpath = "(//input[@name='dtPickerCurrentDate'])[3]")
	public WebElement DatePickerJS;
	
	@FindBy(xpath = "//button[contains(text(),'Submit')]")
	public WebElement RQSubmitButton;
	
	@FindBy(xpath = "//div[@class='kit-buttons']/button[contains(text(),'Cancel')]")
	public WebElement RQCancelButton;
	
	//-------------------------------------------Methods--------------------------------------------//
	
	public int StatusStep() throws InterruptedException {
		DashboardPage dashboard = new DashboardPage(driver);
		if(isElementExisting(driver, dashboard.StatusLabel)) {
			String Act = dashboard.StatusLabel.getText();
			System.out.println(Act);
			String[] curStep = Act.split(" ");
			System.out.println(curStep[1]);
			return Integer.parseInt(curStep[1]);
		}else {
			return 0;
		}
	}
	
	public void requestKit(String action, ExtentTest test) throws InterruptedException {
		waitForElementToDisplay(LstDayWrkdLabel);
		verifyElementTextContains(LstDayWrkdLabel, "Last Day Worked", test);
		DatePickerLDW.sendKeys(CurDateManipulator(1,2,0));
		DatePickerBCD.sendKeys(CurDateManipulator(2,3,0));
		clickElement(JSBeneNoRadio);
		DatePickerJS.sendKeys(CurDateManipulator(15,0,-1));
		if(action == "Cancel") {
			waitForElementToDisplay(RQCancelButton);
			clickElement(RQCancelButton);
		} else if(action == "Submit") {
			waitForElementToDisplay(RQSubmitButton);
			clickElement(RQSubmitButton);
			}
		waitForElementToDisplay(BenefitCommencementLabel);
		verifyElementTextContains(BenefitCommencementLabel, "Benefit Commencement", test);
	}
	
	
	//------------------------------------------Date Manipulator------------------------------------------//
	
	/**
	 * @author - Akshay Tilak
	 */

	public static String CurDateManipulator(Integer day, Integer month, Integer year) {
		
		Date currentDate = new Date();
		//System.out.println(dateFormat.format(currentDate));

		// convert date to calendar
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);

		// manipulate date
		c.add(Calendar.YEAR, year);
		c.add(Calendar.MONTH, month);
		c.add(Calendar.DATE, day);

		// convert calendar to date
		Date currentDateManp = c.getTime();

		// convert date to string
		String reqDate = dateFormat.format(currentDateManp);

		return reqDate;
	}
}

package pages.regression;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.doubleClickElement;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.switchToWindowByTitle;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextContains;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import driverfactory.Driver;

public class PensionAdmin {
	WebDriver driver;

	public PensionAdmin(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	//------------------------------------------Elements--------------------------------------------//
	
	@FindBy(xpath = "//h2[contains(text(),'Employee Search')]")
	public WebElement EmployeeSearchLabel;
	
	@FindBy(xpath = "//input[@name='SsnText']")
	public WebElement ssnTextBox;
	
	@FindBy(xpath = "(//button[@title='Submit'])[2]")
	public WebElement Submit_ssn_Btn;
	
	@FindBy(xpath = "//h4[contains(text(),'Overview')]")
	public WebElement OverviewLabel;
	
	@FindBy(xpath = "//a[contains(text(),'Retirement Online')]")
	public WebElement RetirementOnlineTab;
	
	@FindBy(xpath = "//ul[@id='planNavBar']")
	public WebElement PlanNavBar;
	
	@FindBy(xpath = "(//ul[@id='planNavBar']/li/a)[1]")
	public WebElement PlanTab_1;
	
	@FindBy(xpath = "(//ul[@id='planNavBar']/li/a)[2]")
	public WebElement PlanTab_2;
	
	@FindBy(xpath = "(//ul[@id='planNavBar']/li/a)[3]")
	public WebElement PlanTab_3;
	
	@FindBy(xpath = "//h3[@class='mul-hdr-rule mul-hdr-icons ng-binding']")
	public WebElement planHeader;
	
	@FindBy(xpath = "(//label[contains(text(),'Calc Result')])[1]")
	public WebElement CalcResultlabel;
	
	@FindBy(xpath = "//li[@class='ng-binding']")
	public WebElement CalcResult;
	
	@FindBy(xpath = "//div[@class='caption']/select")
	public WebElement SelectCalcIDDropdown;
	
	@FindBy(xpath = "//option[contains(text(),'511863')]")
	public WebElement CalcIDOption;
	
	public WebElement SaveButton(WebDriver driver, String Num) {
		WebElement saveOption = driver.findElement(By.xpath("(//button[contains(text(),'Save')])["+ Num +"]"));
		return saveOption;
	}
	
	public WebElement SavePopButton(WebDriver driver, String Num) {
		WebElement saveOption = driver.findElement(By.xpath("(//body/div/div//div[2]/button[contains(text(),'Save')])["+ Num +"]"));
		return saveOption;
	}
	
	public WebElement CancelButton(WebDriver driver, String Num) {
		WebElement cancelOption = driver.findElement(By.xpath("(//button[contains(text(),'Cancel')])["+ Num +"]"));
		return cancelOption;
	}
	
	@FindBy(xpath = "(//span[@class='ui-dialog-title ng-binding'])[2]")
	public WebElement ApproveCalcStatusHeader;

	@FindBy(xpath = "//div[@class='mul-box maintenance-employee-docs-detail ng-scope']/div/div/h6[@class='mul-message-hdr ng-binding']")
	public WebElement SuccessMsg_Calc;
	
	public WebElement DOC_Approval(WebDriver driver, int Dropdown_Num) {
		WebElement Doc = driver.findElement(By.xpath("(//select[@id='select'])["+ Dropdown_Num +"]"));
		return Doc;
	}
	
	@FindBy(xpath = "//tbody/tr")
	public WebElement DrpDwns;
	
	public WebElement DOC_Approval_Option(WebDriver driver, String Option) {
		WebElement DocOption = driver.findElement(By.xpath("//option[contains(text(),'"+ Option +"')]"));
		return DocOption;
	}
	
	@FindBy(xpath = "//div[@class='mul-box mul-box-rnd ng-isolate-scope mul-confirmation-ctn']/div/h6[@class='mul-message-hdr ng-binding']")
	public WebElement SuccessMsg_Doc;

	@FindBy(css = "a[title='Cancel Commencement']")
	public WebElement CancelCommencementLink;
	
	@FindBy(xpath = "//body/div/div/div/span[contains(text(),'Approve Cancel Commencement')]")
	public WebElement ApproveCancelCommencement_PopupLabel;
	
	@FindBy(xpath = "(//body/div/div//div[2]/button[contains(text(),'Yes')])[1]")
	public WebElement YesButton;
	
	@FindBy(xpath = "(//body/div/div//div[2]/button[contains(text(),'No')])[1]")
	public WebElement NoButton;
	
	@FindBy(xpath = "//div[@class='tab-pane active']/div/div/h6[@class='mul-message-hdr ng-binding']")
	public WebElement SuccessMsg_CancelComm;
	
	@FindBy(xpath = "//a[@title='Back to search']")
	public WebElement BackToSearch;
	
	
	//-------------------------------------------Methods--------------------------------------------//
	
	public void changeCalcStatus(WebDriver driver, String url, String ssn, String plan, ExtentTest test) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.manage().window().maximize();
		driver.get(url);
		waitForElementToDisplay(EmployeeSearchLabel);
		verifyElementTextContains(EmployeeSearchLabel, "Employee Search", test);
		setInput(ssnTextBox, ssn);
		clickElement(Submit_ssn_Btn);
		waitForElementToDisplay(OverviewLabel);
		verifyElementTextContains(OverviewLabel, "Overview", test);
		clickElement(RetirementOnlineTab);
		if(plan=="CPP")
		{
			waitForElementToDisplay(PlanTab_1);
			clickElement(PlanTab_1);
		}else if(plan == "") {
			waitForElementToDisplay(PlanTab_2);
			clickElement(PlanTab_2);
		}else if(plan == "") {
			waitForElementToDisplay(PlanTab_3);
			clickElement(PlanTab_3);
		}
		waitForElementToDisplay(CalcResultlabel);
		String status = CalcResult.getText().trim();
		System.out.println(status);
		if(status.contains("Red")) {
			waitForElementToDisplay(SelectCalcIDDropdown);
			clickElement(SelectCalcIDDropdown);
			clickElement(CalcIDOption);
			waitForElementToDisplay(SaveButton(driver, "1"));
			clickElement(SaveButton(driver, "1"));
			waitForElementToDisplay(ApproveCalcStatusHeader);
			verifyElementIsPresent(ApproveCalcStatusHeader, test, "Approve Calc Status Header");
			if(isElementExisting(driver, SavePopButton(driver, "1"))) {
				clickElement(SavePopButton(driver, "1"));
			}else if(isElementExisting(driver, SavePopButton(driver, "2"))) {
				clickElement(SavePopButton(driver, "2"));
			}
			waitForElementToDisplay(SuccessMsg_Calc);
			verifyElementIsPresent(SuccessMsg_Calc, test, "Sucsess Msg Header");
			
		}else if(status.contains("Yellow") || status.contains("Green"))
		{
			test.pass("Calc Status : "+ status);
		}
		clickElement(BackToSearch);
		driver.close();
		switchToWindowByTitle("Mercer BenefitsCentral", driver);
	}
	
	public void DocumentApproval(WebDriver driver, String url, String ssn, String plan, ExtentTest test) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.manage().window().maximize();
		driver.get(url);
		waitForElementToDisplay(EmployeeSearchLabel);
		verifyElementTextContains(EmployeeSearchLabel, "Employee Search", test);
		setInput(ssnTextBox, ssn);
		clickElement(Submit_ssn_Btn);
		waitForElementToDisplay(OverviewLabel);
		verifyElementTextContains(OverviewLabel, "Overview", test);
		clickElement(RetirementOnlineTab);
		if(plan=="CPP")
		{
			waitForElementToDisplay(PlanTab_1);
			clickElement(PlanTab_1);
		}else if(plan == "") {
			waitForElementToDisplay(PlanTab_2);
			clickElement(PlanTab_2);
		}else if(plan == "") {
			waitForElementToDisplay(PlanTab_3);
			clickElement(PlanTab_3);
		}
		waitForElementToDisplay(DOC_Approval(driver, 1));
		List<WebElement> boxes = DrpDwns.findElements(By.xpath("//td[5]/select"));
		for (int i = 1; i < boxes.size()+1; i++)
		{
			waitForElementToDisplay(DOC_Approval(driver,  i));
			doubleClickElement(driver, DOC_Approval(driver,  i));
			selEleByVisbleText(DOC_Approval(driver,  i), "Approved");
		    //clickElement(DOC_Approval_Option(driver, "Approved"));
		}
		waitForElementToDisplay(SaveButton(driver, "2"));
		clickElement(SaveButton(driver, "2"));
		if(isElementExisting(driver, SavePopButton(driver, "1"))) {
			clickElement(SavePopButton(driver, "1"));
		}else if(isElementExisting(driver, SavePopButton(driver, "2"))) {
			clickElement(SavePopButton(driver, "2"));
		}
		waitForElementToDisplay(SuccessMsg_Doc);
		verifyElementIsPresent(SuccessMsg_Doc, test, "Sucsess Msg Header");		
		clickElement(BackToSearch);
		driver.close();
		switchToWindowByTitle("Mercer BenefitsCentral", driver);
	}
		
	
	public void cancelCommencement(WebDriver driver, String url, String ssn, ExtentTest test) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.manage().window().maximize();
		driver.get(url);
		waitForElementToDisplay(EmployeeSearchLabel);
		verifyElementTextContains(EmployeeSearchLabel, "Employee Search", test);
		setInput(ssnTextBox, ssn);
		clickElement(Submit_ssn_Btn);
		waitForElementToDisplay(OverviewLabel);
		verifyElementTextContains(OverviewLabel, "Overview", test);
		clickElement(RetirementOnlineTab);
		//List<WebElement> p_tabs = PlanNavBar.findElements(By.xpath("/li/a"));
		List<WebElement> p_tabs = driver.findElements(By.xpath("//ul[@id='planNavBar']/li/a"));
		int no_tabs = p_tabs.size();
		System.out.println(no_tabs);
		for(int i=1;i<=no_tabs;i++) {
			
			waitForElementToDisplay(By.xpath("(//ul[@id='planNavBar']/li/a)["+i+"]"));
			clickElement(driver.findElement(By.xpath("(//ul[@id='planNavBar']/li/a)["+i+"]")));
			
			waitForElementToDisplay(planHeader);
			if(isElementExisting(driver, CancelCommencementLink)){
				clickElement(CancelCommencementLink);
				waitForElementToDisplay(ApproveCancelCommencement_PopupLabel);
				verifyElementIsPresent(ApproveCancelCommencement_PopupLabel, test, "Approve Calc Status Header");
				clickElement(YesButton);
				waitForElementToDisplay(SuccessMsg_CancelComm);
				verifyElementIsPresent(SuccessMsg_CancelComm, test, "Success Msg Header");
			}else {
				continue;
			}
		}
		clickElement(BackToSearch);
		driver.close();
		switchToWindowByTitle("Mercer BenefitsCentral", driver);
	}
}

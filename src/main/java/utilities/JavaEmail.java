package utilities;




import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
//https://myaccount.google.com/lesssecureapps
public class JavaEmail {

Properties emailProperties;
Session mailSession;
MimeMessage emailMessage;

public static void sendEmail() throws AddressException,
MessagingException {

	try
	{
JavaEmail javaEmail = new JavaEmail();

javaEmail.setMailServerProperties();
javaEmail.createEmailMessage();
javaEmail.sendMail();
	}
catch(Exception e)
{
	e.printStackTrace();
}
}

public void setMailServerProperties() {

String emailPort = "587";//gmail's smtp port

emailProperties = System.getProperties();
emailProperties.put("mail.smtp.port", emailPort);
emailProperties.put("mail.smtp.auth", "true");
emailProperties.put("mail.smtp.starttls.enable", "true");


}

public void createEmailMessage() throws AddressException,
MessagingException {
String[] toEmails = { "yugandhar.gorrepati@lntinfotech.com","yugi2519@gmail.com" };
String emailSubject = "Automation Test Status";
String emailBody = "All tests are executed, please check reports http://localhost:8080/UiFramework/reports.jsp";

mailSession = Session.getDefaultInstance(emailProperties, null);
emailMessage = new MimeMessage(mailSession);

for (int i = 0; i < toEmails.length; i++) {
emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmails[i]));
}

emailMessage.setSubject(emailSubject);
emailMessage.setContent(emailBody, "text/html");//for a html email
//emailMessage.setText(emailBody);// for a text email

}

public void sendMail() throws AddressException, MessagingException {

String emailHost = "smtp.gmail.com";
String fromUser = "yugi2519@gmail.com";//just the id alone without @gmail.com
String fromUserEmailPassword = "******";

Transport transport = mailSession.getTransport("smtp");

transport.connect(emailHost, fromUser, fromUserEmailPassword);
transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
transport.close();
System.out.println("Email sent successfully.");
}

}